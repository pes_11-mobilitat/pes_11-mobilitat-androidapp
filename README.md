# PES_11-Mobilitat-AndroidApp

## Funcions de UtilsSessions

**int registreUsuari(Usuari user)**
returns:
- (-1) email existent
- (0) OK
- (-2) Altres errors. No hauria d'arribar si es fa l'input bé i el server està ON.

**int editUsuari(Usuari user)**
returns:
- (-1) enviar a pàgina de login. Tornar a loggejar
- (0) OK
- (-2) Altres errors. No hauria d'arribar si es fa l'input bé i el server està ON.
 
**Usuari getUser()**
returns:
- Usuari  

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**int eliminarUsuari(String password)**
returns:
- (-1) password incorrecte
- (-2) tokens caducats. enviar a login i tornar a fer login (crec que no hauria de passar mai)
- (0) OK
- (-3) Altres errors. No hauria d'arribar si es fa l'input bé i el server està ON.

**int loginUsuari(String email, String password)**
returns:
- (-1) mail o password incorrecte
- (0) OK
- (-2) Altres errors. No hauria d'arribar si es fa l'input bé i el server està ON.

**void logoutUsuari()**
sempre hauria de funcionar

## Funcions de UtilsUbicacio

**void pushUbicacio(LatLng ubi)**  
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**LatLng getUbicacio(String userId)**
returns:
- LatLng (ubicacio en format Latitud-Longitud) 

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

## Funcions de UtilsMatch

**void newHelpRequest(LatLng ubi, String tipus_request, Integer duracio_estimada)**  
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**AjudaImmediata getNearRequest(LatLng ubi)**
Aquesta funció la llençarà el voluntari quan li arribi la notificació d'una demanda d'ajuda propera i hagi d'obtenir les dades d'aquesta ajuda
returns:
- AjudaImmediata (classe amb totes les dades necessaries d'una demanda d'ajuda immediata, amb getters i setters)  

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*  
*S'ha de tractar amb try/catch de l'exception NoSuchFieldException. Si es llança l'excepcio, es manté el home buit.*

**AjudaImmediata getCurrentRequest()**
(?) Aquesta funció la llançarà el voluntari quan hagi fet una demanda d'ajuda, per veure la informació de l'ajuda mentre espera
returns:
- AjudaImmediata (classe amb totes les dades necessaries d'una demanda d'ajuda immediata, amb getters i setters) 

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Integer acceptRequest(String request_id)**
returns:
- (0) OK
- (-1) Request ja acceptada per algú altre  

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**String getIdVoluntariMatch()**
returns:
- String id del voluntari del match (la cridarà l'upm)

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**String getIdMatch()**
returns:
- String id del match

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**String getMatch()**
returns:
- String en format json amb la info del match.

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**String endMatch(String match_id, String valoracio)**
Aquesta funcio es crida quan l'upm vlaora al voluntari. Valoracio sera "1" si ha valorat positivament i "-1" si ho ha fet negativament

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

## Funcions de UtilsEsdeveniment

La creadora d'Esdeveniment té els paràmetres: String title, Double lat, Double lon, Integer daytime, String date, String uid, String descripcio

**void newEvent(Esdeveniment)**
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**void editEvent(Esdeveniment)**
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**void deleteEvent(Esdeveniment)**
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**void joinEvent(Esdeveniment)**
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**void leaveEvent(Esdeveniment)**
*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Array<Esdeveniment> getEventsUPM()**
returns:
- Array d'esdeveniments (poca info de cada esdeveniment)

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Array<Esdeveniment> getEventsVoluntari()**
returns:
- Array d'esdeveniments (poca info de cada esdeveniment)

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Array<Esdeveniment> getEventsFiltered(String date, Integer daytime, LatLng ev_ubi, Double radi)**
Qualsevol dels paràmetres d'input poden ser nulls.
returns:
- Array d'esdeveniments (poca info de cada esdeveniment). 

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Esdeveniment getEventPrivate(String eventId)**
returns:
- Esdeveniment (info completa)

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*

**Esdeveniment getEventPublic(String eventId)**
returns:
- Esdeveniment (info completa)

*S'ha de tractar amb try/catch de l'exception AuthenticatorException. Si es llança l'excepcio, redirigir a login.*