package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import static com.example.aidbee.UtilsSessions.refreshToken;

public class utilsMatch {
    public static void newHelpRequest(LatLng ubi, String type_req, Integer estim_dur) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String latitud = String.valueOf(ubi.latitude);
        String longitud = String.valueOf(ubi.longitude);
        asyncStatus.execute("newhelprequest", userId, tokenId, latitud, longitud,  type_req, estim_dur.toString(), "https://us-central1-pes11-mobilitat.cloudfunctions.net/NewHelpRequest");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("newhelprequest", userId, tokenId, latitud, longitud, type_req, estim_dur.toString(), "https://us-central1-pes11-mobilitat.cloudfunctions.net/NewHelpRequest");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static int acceptRequest(String request_id) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        asyncStatus.execute("acceptrequest", userId, tokenId, request_id, "https://us-central1-pes11-mobilitat.cloudfunctions.net/AcceptRequest");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("acceptrequest", userId, tokenId, request_id, "https://us-central1-pes11-mobilitat.cloudfunctions.net/AcceptRequest");
                try {
                    status = asyncStatus2.get();
                    if (status == 200) return (0);
                    else return (-1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (status == 200) return(0);
            else return (-1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return(-2); //no hauria d'arribar aqui
    }

    public static AjudaImmediata getNearRequest(LatLng ubi) throws AuthenticatorException, NoSuchFieldException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String userId = sharedPreferences.getString("userId", "0");
        String stringNearReq;
        Integer status;
        String latitud = String.valueOf(ubi.latitude);
        String longitud = String.valueOf(ubi.longitude);
        asyncMessage.execute("getnearrequest", userId, idToken, latitud, longitud, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetNearRequest");
        try {
            stringNearReq = asyncMessage.get();
            if (stringNearReq.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("getnearrequest", userId, idToken, latitud, longitud, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetNearRequest");
                    try {
                        stringNearReq = asyncMessage1.get();
                        if (stringNearReq.equals("{}")) throw new NoSuchFieldException();
                        ObjectMapper mapper = new ObjectMapper();
                        AjudaImmediata AjIm = mapper.readValue(stringNearReq, AjudaImmediata.class);
                        return AjIm;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (stringNearReq.equals("{}")) throw new NoSuchFieldException();
                ObjectMapper mapper = new ObjectMapper();
                AjudaImmediata AjIm = mapper.readValue(stringNearReq, AjudaImmediata.class);
                return AjIm;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        AjudaImmediata AjIm= null;
        return AjIm; //no hauria d'arribar mai aqui
    }

    public static AjudaImmediata geCurrentRequest() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String userId = sharedPreferences.getString("userId", "0");
        String stringCurrentReq;
        Integer status;
        asyncMessage.execute("getcurrentrequest", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetCurrentRequest");
        try {
            stringCurrentReq = asyncMessage.get();
            if (stringCurrentReq.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("getcurrentrequest", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetCurrentRequest");
                    try {
                        stringCurrentReq = asyncMessage1.get();
                        ObjectMapper mapper = new ObjectMapper();
                        AjudaImmediata AjIm = mapper.readValue(stringCurrentReq, AjudaImmediata.class);
                        return AjIm;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ObjectMapper mapper = new ObjectMapper();
                AjudaImmediata AjIm = mapper.readValue(stringCurrentReq, AjudaImmediata.class);
                return AjIm;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        AjudaImmediata AjIm= null;
        return AjIm; //no hauria d'arribar mai aqui
    }

    public static String getIdVoluntariMatch() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String voluntariId = null;
        String messages;
        asyncMessage.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
        try {
            messages = asyncMessage.get();
            if (messages.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
                try {
                    messages = asyncMessage2.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    voluntariId = jsonMsg.getString("VolunteerId");
                    return voluntariId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    messages = asyncMessage.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    voluntariId = jsonMsg.getString("uid");
                    return voluntariId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (voluntariId); //no hauria d'arribar aqui
    }

    public static String getIdMatch() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String matchId = null;
        String messages;
        asyncMessage.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
        try {
            messages = asyncMessage.get();
            if (messages.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
                try {
                    messages = asyncMessage2.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    matchId = jsonMsg.getString("match_id");
                    return matchId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    messages = asyncMessage.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    matchId = jsonMsg.getString("match_id");
                    return matchId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (matchId); //no hauria d'arribar aqui
    }

    public static String getMatch() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String messages = null;
        asyncMessage.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
        try {
            messages = asyncMessage.get();
            if (messages.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
                try {
                    messages = asyncMessage2.get();
                    return messages;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    messages = asyncMessage.get();
                    return messages;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (messages); //no hauria d'arribar aqui
    }

    public static int endMatch(String match_id, String valoracio) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        asyncStatus.execute("endmatch", userId, tokenId, match_id, valoracio, "https://us-central1-pes11-mobilitat.cloudfunctions.net/FinalizeMatch");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("endmatch", userId, tokenId, match_id, valoracio, "https://us-central1-pes11-mobilitat.cloudfunctions.net/FinalizeMatch");
                try {
                    status = asyncStatus2.get();
                    if (status == 200) return (0);
                    else return (-1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (status == 200) return(0);
            else return (-1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return(-2); //no hauria d'arribar aqui
    }



}
