package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import static com.example.aidbee.UtilsSessions.refreshToken;

public class UtilsEsdeveniments {
    public static void newEvent(Esdeveniment ev) throws JsonProcessingException, AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        ObjectMapper mapper = new ObjectMapper();
        String eventinfo = mapper.writeValueAsString(ev);
        asyncStatus.execute("newevent", userId, eventinfo, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/NewEvent");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("newevent", userId, eventinfo, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/NewEvent");
                status = asyncStatus2.get();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static void editEvent(Esdeveniment ev) throws JsonProcessingException, AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        ObjectMapper mapper = new ObjectMapper();
        String eventinfo = mapper.writeValueAsString(ev);
        String eventId = ev.getId();
        asyncStatus.execute("editevent", userId, eventId, eventinfo, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/EditEvent");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("editevent", userId, eventId, eventinfo, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/EditEvent");
                status = asyncStatus2.get();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static void deleteEvent(Esdeveniment ev) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String eventId = ev.getId();
        asyncStatus.execute("deleteevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteEvent");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("deleteevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteEvent");
                status = asyncStatus2.get();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static void joinEvent(Esdeveniment ev) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String eventId = ev.getId();
        asyncStatus.execute("joinevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/JoinEvent");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("joinevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/JoinEvent");
                status = asyncStatus2.get();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static void leaveEvent(Esdeveniment ev) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String eventId = ev.getId();
        asyncStatus.execute("leaveevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/LeaveEvent");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("leaveevent", userId, eventId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/LeaveEvent");
                status = asyncStatus2.get();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static Esdeveniment getEventPrivate(String eventId) throws AuthenticatorException, NoSuchFieldException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String userId = sharedPreferences.getString("userId", "0");
        String stringPrivateEvent;
        Integer status;
        asyncMessage.execute("geteventprivate", idToken, userId, eventId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPrivate");
        try {
            stringPrivateEvent = asyncMessage.get();
            if (stringPrivateEvent.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("geteventprivate", idToken, userId, eventId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPrivate");
                    try {
                        stringPrivateEvent = asyncMessage1.get();
                        if (stringPrivateEvent.equals("{}")) throw new NoSuchFieldException();
                        ObjectMapper mapper = new ObjectMapper();
                        Esdeveniment ev = mapper.readValue(stringPrivateEvent, Esdeveniment.class);
                        return ev;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (stringPrivateEvent.equals("{}")) throw new NoSuchFieldException();
                ObjectMapper mapper = new ObjectMapper();
                Esdeveniment ev = mapper.readValue(stringPrivateEvent, Esdeveniment.class);
                return ev;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        Esdeveniment ev= null;
        return ev; //no hauria d'arribar mai aqui
    }

    public static Esdeveniment getEventPublic(String eventId) throws AuthenticatorException, NoSuchFieldException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String stringPublicEvent;
        Integer status;
        asyncMessage.execute("geteventpublic", idToken, eventId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPublic");
        try {
            stringPublicEvent = asyncMessage.get();
            if (stringPublicEvent.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("geteventpublic", idToken, eventId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPublic");
                    try {
                        stringPublicEvent = asyncMessage1.get();
                        if (stringPublicEvent.equals("{}")) throw new NoSuchFieldException();
                        ObjectMapper mapper = new ObjectMapper();
                        Esdeveniment ev = mapper.readValue(stringPublicEvent, Esdeveniment.class);
                        return ev;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (stringPublicEvent.equals("{}")) throw new NoSuchFieldException();
                ObjectMapper mapper = new ObjectMapper();
                Esdeveniment ev = mapper.readValue(stringPublicEvent, Esdeveniment.class);
                return ev;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        Esdeveniment ev= null;
        return ev; //no hauria d'arribar mai aqui
    }

    public static ArrayList<Esdeveniment> getEventsUPM() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String esdeveniments;
        ArrayList<Esdeveniment> arrayEv = new ArrayList<Esdeveniment>();
        asyncMessage.execute("geteventsupm", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsUPM");
        try {
            esdeveniments = asyncMessage.get();
            if (esdeveniments.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("geteventsupm", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsUPM");
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(arrayEv.toString());
        return arrayEv;
    }

    public static ArrayList<Esdeveniment> getEventsVoluntari() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String esdeveniments;
        ArrayList<Esdeveniment> arrayEv = new ArrayList<Esdeveniment>();
        asyncMessage.execute("geteventsvoluntari", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsVolunteer");
        try {
            esdeveniments = asyncMessage.get();
            if (esdeveniments.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("geteventsvoluntari", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsVolunteer");
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayEv;
    }

    public static ArrayList<Esdeveniment> getEventsFiltered(String date, Integer daytime, LatLng ev_ubi, Double distancia) throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String esdeveniments;
        String latitud = null;
        String longitud = null;
        if (ev_ubi != null) {
            latitud = String.valueOf(ev_ubi.latitude);
            longitud = String.valueOf(ev_ubi.longitude);
        }
        ArrayList<Esdeveniment> arrayEv = new ArrayList<Esdeveniment>();
        asyncMessage.execute("geteventsfiltered", tokenId, date, String.valueOf(daytime), latitud, longitud, String.valueOf(distancia), "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetPublicEventsFiltered");
        try {
            esdeveniments = asyncMessage.get();
            if (esdeveniments.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("geteventsfiltered", tokenId, date, String.valueOf(daytime), latitud, longitud, String.valueOf(distancia), "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetPublicEventsFiltered");
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    esdeveniments = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONArray jArr = new JSONArray(esdeveniments.trim());
                    for(int i=0; i < jArr.length(); i++) {
                        JSONObject jObj = jArr.getJSONObject(i);
                        Esdeveniment ev = mapper.readValue(jObj.toString(), Esdeveniment.class);
                        arrayEv.add(ev);
                    }
                    return arrayEv;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayEv;
    }
}
