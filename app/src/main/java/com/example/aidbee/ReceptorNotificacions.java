package com.example.aidbee;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static android.content.ContentValues.TAG;

public class ReceptorNotificacions extends FirebaseMessagingService {

    private String codeNotification = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        super.onMessageReceived(remoteMessage);
        //showNotificacion(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            codeNotification = remoteMessage.getData().get("codi");
            if (codeNotification != null) {
                if (codeNotification.equals("20")) {
                    FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(3);
                } else if (codeNotification.equals("30")) { //code 30
                    FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(2);
                } else if (codeNotification.equals("60")) {
                    FactoryStatusApplication.getInstance().getStatusApplication().setRefreshMessage(true);
                } else if (codeNotification.equals("40")) {
                    if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() % 2 == 0) {
                        FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(0);
                    } else
                        FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(1);
                } else if (codeNotification.equals("50")) {
                    if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() == 5) {
                        FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(7);
                        String resultat = remoteMessage.getData().get("valoracio");
                        PantallaValoracioAjudant.setResultat(resultat);
                    }
                }
            }

        }
        sendNotificacion(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
        //    if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
         //       scheduleJob();
        //    } else {
                // Handle message within 10 seconds
        //        handleNow();


        // Check if message contains a notification payload.
        //if (remoteMessage.getNotification() != null) {
        //    Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        //}

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void sendNotificacion(String title, String body) {
        Intent intent = new Intent(this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "MyNotifications";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.logo_aidbee)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    public void showNotificacion(String title, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "MyNotifications")
                .setContentTitle(title)
                .setSmallIcon(R.drawable.logo_aidbee)
                .setAutoCancel(true)
                .setContentText(message);

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(999, builder.build());
    }


}
