package com.example.aidbee;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class UtilsAltres {
    public static ArrayList<LatLng> getEsports() throws IOException, JSONException {
        ArrayList<LatLng> arrayUbicacions =null;
        AsyncMessage asyncMessage = new AsyncMessage();
        asyncMessage.execute("getesports", "http://10.4.41.144:3000/event/reduced/future");
        String response = null;
        try {
            response = asyncMessage.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONArray jArr = new JSONArray(response.trim());
        for(int i=0; i < jArr.length(); i++) {
            JSONObject jObj = jArr.getJSONObject(i);
            LatLng latlng = new LatLng(Double.parseDouble(jObj.getString("latitude")), Double.parseDouble(jObj.getString("longitude")));
            arrayUbicacions.add(latlng);
        }
        return arrayUbicacions;
    }
}
