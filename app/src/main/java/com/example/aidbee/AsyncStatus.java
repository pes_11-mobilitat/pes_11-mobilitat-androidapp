package com.example.aidbee;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;


public class AsyncStatus extends AsyncTask<String, Void, Integer>{

    @Override
    protected Integer doInBackground(String... string) {
        Integer status = null;
        if (string[0].equals("register")) {
            try {
                JSONObject jsonIn = new JSONObject(string[1]);
                JSONObject jsonOut = new JSONObject();
                String email = jsonIn.getString("email");
                String pass = jsonIn.getString("password");
                try {
                    jsonOut.put("email",email);
                    jsonOut.put("password", pass);
                    jsonOut.put("returnSecureToken", true);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[2]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(jsonResponse);
                    String idToken = jsonResponse.getString("idToken");
                    String refreshToken = jsonResponse.getString("refreshToken");
                    String userId = jsonResponse.getString("localId");
                    Context context = SplashActivity.getContext();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("idToken", idToken);
                    editor.putString("refreshToken", refreshToken);
                    editor.putString("userId", userId);
                    editor.putString("email", email);
                    editor.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("login")) {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("email",string[1]);
                    jsonOut.put("password", string[2]);
                    jsonOut.put("returnSecureToken", true);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    System.out.println(jsonResponse);
                    String idToken = jsonResponse.getString("idToken");
                    String refreshToken = jsonResponse.getString("refreshToken");
                    String userId = jsonResponse.getString("localId");
                    Context context = SplashActivity.getContext();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("idToken", idToken);
                    editor.putString("refreshToken", refreshToken);
                    editor.putString("userId", userId);
                    editor.putString("email", string[1]);
                    editor.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
        } else if (string[0].equals("refreshToken")) {
            String urlParameters  = "grant_type=refresh_token&refresh_token="+string[1];
            HttpURLConnection con = null;
            try {
                //Create connection
                URL url = new URL(string[2]);
                con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                con.setRequestProperty("Accept", "application/x-www-form-urlencoded");
                //con.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
                con.setDoOutput(true);
                con.setDoInput(true);
                con.connect();

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        con.getOutputStream ());
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                Log.i("STATUS", String.valueOf(con.getResponseCode()));
                Log.i("MSG" , con.getResponseMessage());

                //Get Response
                InputStream is;
                status = con.getResponseCode();
                if (status != HttpURLConnection.HTTP_OK) {
                    is = con.getErrorStream();
                    return status;
                }
                else {
                    is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                    JSONObject jsonResponse = new JSONObject(response.toString());
                    String idToken = jsonResponse.getString("access_token");
                    String refreshToken = jsonResponse.getString("refresh_token");
                    Context context = SplashActivity.getContext();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("idToken", idToken);
                    editor.putString("refreshToken", refreshToken);
                    editor.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(con != null) {
                    con.disconnect();
                }
            }
        } else if (string[0].equals("newuser") || string[0].equals("edituser")) {
            System.out.println(string[2]);
            try {
                JSONObject jsonIn = new JSONObject(string[2]);
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("profileInfo", jsonIn);
                    System.out.println(jsonOut);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK) {
                        is = con.getErrorStream();
                    }
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("deleteuser") || string[0].equals("logout")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("updateubicacio")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("lat", string[3]);
                    jsonOut.put("lon", string[4]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[5]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("newhelprequest")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("request_type", string[5]);
                    jsonOut.put("estimated_duration", string[6]);
                    jsonOut.put("location_lat", string[3]);
                    jsonOut.put("location_lon", string[4]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[7]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("acceptrequest")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("request_id", string[3]);
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("endmatch")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("match_id", string[3]);
                    jsonOut.put("valoracio", Integer.parseInt(string[4]));
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[5]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }  else if (string[0].equals("sendMessage")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("cid", string[2]);
                    jsonOut.put("msg", string[4]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[5]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("setdevicetoken")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("device_token", string[2]);
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("newevent")) {
            try {
                JSONObject jsonOut = new JSONObject();
                JSONObject eventInfo = new JSONObject(string[2]);
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("eventinfo", eventInfo);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("editevent")) {
            try {
                JSONObject jsonOut = new JSONObject();
                JSONObject eventInfo = new JSONObject(string[3]);
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("eventid", string[2]);
                    jsonOut.put("eventinfo", eventInfo);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[5]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[4]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("deleteevent")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("eventid", string[2]);
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("joinevent")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("eventid", string[2]);
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("leaveevent")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("eventid", string[2]);
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return status;
    }
}
