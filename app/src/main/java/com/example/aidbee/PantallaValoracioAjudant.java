package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class PantallaValoracioAjudant extends AppCompatActivity {
    Button botoOk;
    TextView missatgeValoracio;
    ImageView imatgeValoracio;
    private static String resultat;

    private static AppCompatActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.pantalla_valoracio_ajudant);
        //soc un comentari inutil

        
        botoOk = findViewById(R.id.botoOk);

        //establir el text
        missatgeValoracio = findViewById(R.id.text_valoració1);
        imatgeValoracio = findViewById(R.id.imatge_valoracio);
        //consultar valoració positiva o negativa


        if (resultat.equals("1")) {
            missatgeValoracio.setText(getResources().getString(R.string.t_han_valorat_positivament));
            imatgeValoracio.setImageDrawable(getResources().getDrawable(R.drawable.valoracio_positiva));
        }
        else {
            missatgeValoracio.setText(getResources().getString(R.string.valoracio_negativa));
            imatgeValoracio.setImageDrawable(getResources().getDrawable(R.drawable.valoracio_negativa));
        }

        botoOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(1);
                startActivity(new Intent(PantallaValoracioAjudant.this, Home.class)); //això hauria de canviar a la pantalla de home
            }
        });
    }

    static public void setResultat(String res) {
        resultat = res;
    }
}
