package com.example.aidbee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyDaysSpinnerAdapter extends ArrayAdapter<StateVO> {
    private Context mContext;
    private ArrayList<StateVO> listState;
    private MyDaysSpinnerAdapter myAdapter;
    private boolean isFromView = false;

    public MyDaysSpinnerAdapter(Context context, int resource, List<StateVO> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<StateVO>) objects;
        this.myAdapter = this;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item_days, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox);
            holder.mEditText_HINI = convertView
                    .findViewById(R.id.hIni);
            holder.mEditText_FIN = convertView
                    .findViewById(R.id.hFi);
            holder.mTextView2 = convertView
                    .findViewById(R.id.text2);
            holder.mTextView3 = convertView
                    .findViewById(R.id.text3);
            holder.mTextViewDe = convertView
                    .findViewById(R.id.textDe);
            holder.mTextViewA = convertView
                    .findViewById(R.id.textA);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
            holder.mEditText_HINI.setVisibility(View.INVISIBLE);
            holder.mEditText_FIN.setVisibility(View.INVISIBLE);
            holder.mTextView2.setVisibility(View.INVISIBLE);
            holder.mTextView3.setVisibility(View.INVISIBLE);
            holder.mTextViewDe.setVisibility(View.INVISIBLE);
            holder.mTextViewA.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                    listState.get(position).sethIni(holder.mEditText_HINI.getText().toString());
                    listState.get(position).sethFi(holder.mEditText_FIN.getText().toString());
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
        private EditText mEditText_HINI;
        private EditText mEditText_FIN;
        private TextView mTextView2;
        private TextView mTextView3;
        private TextView mTextViewDe;
        private TextView mTextViewA;
    }
}