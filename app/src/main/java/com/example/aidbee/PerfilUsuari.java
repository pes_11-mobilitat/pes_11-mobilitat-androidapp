package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import de.hdodenhof.circleimageview.CircleImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.aidbee.UtilsSessions.editUsuari;
import static com.example.aidbee.UtilsSessions.getUser;
import static com.example.aidbee.UtilsSessions.refreshToken;


public class PerfilUsuari extends AppCompatActivity {

    TextView viewGender, textDisc, textDisp, textModificarPassword, textSexe;
    CircleImageView profilePic;
    EditText inTelefon, inUsuari, inMail, inDiscapacitat, inBirthday,inPasswordAntiga, inPassword, inPasswordConfirm, inNom;
    Button buttonEditar, buttonEditarPhoto;
    Spinner spinnerGender, spinnerDisponibilitat;
    Usuari user;
    private StorageReference mStorage;
    private Uri selectedImage;

    //Necessari per Spinner Disponibilitat
    String[] select_days;
    ArrayList<StateVO> listVOs;
    ArrayList<DisponibilitatDia> disponibilitats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.select_days = new String[]{getResources().getString(R.string.availability), getResources().getString(R.string.dilluns), getResources().getString(R.string.dimarts), getResources().getString(R.string.dimecres), getResources().getString(R.string.dijous), getResources().getString(R.string.divendres), getResources().getString(R.string.dissabte), getResources().getString(R.string.diumenge)};
        setContentView(R.layout.activity_profile_ump_template);
        Integer status = refreshToken();
        if (status.equals(200)) {
            //Storage
            mStorage = FirebaseStorage.getInstance().getReference();
            //Usuari
            try {
                user = getUser();

            } catch (AuthenticatorException e) {
                e.printStackTrace();
            }

            //Toolbar
            Toolbar toolbar = findViewById(R.id.toolbar);
            initToolbar(toolbar);

            //Text views
            viewGender = findViewById(R.id.viewGender);
            textDisc = findViewById(R.id.textDisc);
            textDisp = findViewById(R.id.textDisp);
            textModificarPassword = findViewById(R.id.textModificarPassword);
            textSexe = findViewById(R.id.textSexe);

            //Edit texts
            inTelefon = findViewById(R.id.inTelefon);
            inUsuari = findViewById(R.id.inUsuari);
            inMail = findViewById(R.id.inMail);
            inDiscapacitat = findViewById(R.id.inDiscapacitat);
            inBirthday = findViewById(R.id.inBirthday);
            inPasswordAntiga = findViewById(R.id.inPasswordAntiga);
            inPassword = findViewById(R.id.inPassword);
            inPasswordConfirm = findViewById(R.id.inPasswordConfirm);
            inNom = findViewById(R.id.inNom);

            //SpinnerDisponibilitat
            listVOs = new ArrayList<>();
            spinnerDisponibilitat = findViewById(R.id.spinner);

            //ImageView
            profilePic = findViewById(R.id.profilePic);
            selectedImage = null;

            //Selector data
            inBirthday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getId() == R.id.inBirthday) {
                        showDatePickerDialog(inBirthday);
                    }
                }
            });

            //Selector Gender
            spinnerGender = findViewById(R.id.spinnerGender);
            ArrayAdapter<CharSequence> spinnerArrayAdapter = ArrayAdapter.createFromResource(this, R.array.gender, R.layout.spinner_item);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            spinnerGender.setAdapter(spinnerArrayAdapter);

            //Botó per editar imatge
            buttonEditarPhoto = findViewById(R.id.buttonEditarPhoto);
            buttonEditarPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    startActivityForResult(intent, 1);

                }
            });

            //Botó per confirmar l'edició
            buttonEditar = findViewById(R.id.buttonEditar);
            buttonEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String pass = String.valueOf(inPassword.getText());
                    String passConfirm = String.valueOf(inPasswordConfirm.getText());
                    if (!pass.equals(passConfirm)) {
                        inPasswordConfirm.setError(getResources().getString(R.string.no_coincideix));
                        inPasswordConfirm.requestFocus();
                    } else {

                        //FALTA IMATGE
                        String nomComplet = String.valueOf(inNom.getText());
                        String nomUsuari = String.valueOf(inUsuari.getText());
                        String aniversari = String.valueOf(inBirthday.getText());
                        String mail = String.valueOf(inMail.getText());
                        String telefon = String.valueOf(inTelefon.getText());
                        String sexe = spinnerGender.getSelectedItem().toString();
                        String disc = String.valueOf(inDiscapacitat.getText());

                        String passAntiga = String.valueOf(inPasswordAntiga.getText());
                        String passNova = String.valueOf(inPassword.getText());
                        String passNovaConfirm = String.valueOf(inPasswordConfirm.getText());


                        //FALTA DISTINCIÓ ENTRE TIPUS D'USUARI

                        if (selectedImage != null) {
                            final StorageReference filePath = mStorage.child("images").child(Objects.requireNonNull(selectedImage.getLastPathSegment()));
                            filePath.putFile(selectedImage).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }

                                    // Continue with the task to get the download URL
                                    return filePath.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()) {
                                        Uri downloadUri = task.getResult();
                                        user.setUrlImg(downloadUri.toString());
                                        editUsuari(user);
                                    } else {
                                        Toast.makeText(PerfilUsuari.this, "upload failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        if (!nomComplet.isEmpty()) {
                            user.setNom(nomComplet);
                        }
                        if (!nomUsuari.isEmpty()) {
                            user.setUsuari(nomUsuari);
                        }
                        if (!aniversari.isEmpty()) {
                            user.setAniversari(aniversari);
                        }
                        if (!mail.isEmpty()) {
                            user.setEmail(mail);
                        }
                        if (!telefon.isEmpty()) {
                            user.setTelefon(telefon);
                        }
                        if (!sexe.isEmpty()) {
                            user.setSexe(sexe);
                        }

                        if (!passNova.isEmpty()) {
                            if (passNova.length() >= 6) {
                                if (!passNovaConfirm.isEmpty() && passNova.equals(passNovaConfirm)) {
                                    if (!passAntiga.isEmpty() && passAntiga.equals(user.getPassword())) {
                                        user.setPassword(passNova);
                                    } else {
                                        inPasswordAntiga.setError(getResources().getString(R.string.old_psw));
                                        inPasswordAntiga.requestFocus();
                                    }
                                } else {
                                    inPasswordConfirm.setError(getResources().getString(R.string.no_coincideix));
                                    inPasswordConfirm.requestFocus();
                                }
                            } else {
                                inPassword.setError(getResources().getString(R.string.indica_contrasenya));
                                inPassword.requestFocus();
                            }
                        }
                        if (user.getRol().equals("UPM")) {
                            if (!disc.isEmpty()) {
                                user.setDiscapacitat(disc);
                            }
                        }
                        else {
                            //FALTA AJUDANT
                            disponibilitats = new ArrayList<>();
                            for (int i = 1; i < listVOs.size(); i++) {
                                if (listVOs.get(i).isSelected()) {
                                    DisponibilitatDia aux = new DisponibilitatDia(listVOs.get(i).getTitle(),
                                            listVOs.get(i).gethIni(),
                                            listVOs.get(i).gethFi());
                                    disponibilitats.add(aux);
                                }
                            }
                        }
                        editUsuari(user);
                        finish();
                    }
                }
            });

            for (int i = 0; i < select_days.length; i++) {
                StateVO stateVO = new StateVO();
                stateVO.setTitle(select_days[i]);
                stateVO.setSelected(false);
                stateVO.sethFi("00");
                stateVO.sethFi("00");
                listVOs.add(stateVO);
            }
            MyDaysSpinnerAdapter myAdapter = new MyDaysSpinnerAdapter(this, 0,
                    listVOs);
            spinnerDisponibilitat.setAdapter(myAdapter);

            //Perquè no s'obri el teclat en obrir l'activity
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //CRIDAR AL BACK END PER DEMANAR DADES PERFIL

            String typeUser = user.getRol();
            String Img = user.getUrlImg();
            String NomComplet = user.getNom();
            String Username = user.getUsuari();
            String Birthday = user.getAniversari();
            String email = user.getEmail();
            String phone = user.getTelefon();
            String gender = user.getSexe();

            int spinnerPosition = spinnerArrayAdapter.getPosition(gender);
            spinnerGender.setSelection(spinnerPosition);

            inNom.setHint(NomComplet);
            inUsuari.setHint(Username);
            inMail.setHint(email);
            if (Img != null) {
                String url = user.getUrlImg();
                Glide.with(PerfilUsuari.this)
                        .load(url)
                        .fitCenter()
                        .centerCrop()
                        .into(profilePic);
            }
            if (Birthday != null) {
                inBirthday.setHint(Birthday);
                inBirthday.setVisibility(View.VISIBLE);

            }
            if (phone != null) {
                inTelefon.setHint(phone);
                inTelefon.setVisibility(View.VISIBLE);

            }
            if(gender != null) {
                if (!gender.equals("Prefereixo no contestar") && !gender.equals("Prefer not to answer")) {
                    viewGender.setText(gender);
                    viewGender.setVisibility(View.VISIBLE);

                }
            }

            if (typeUser.equals("UPM")) {
                String disc = user.getDiscapacitat();
                inDiscapacitat.setHint(disc);
                inDiscapacitat.setVisibility(View.VISIBLE);
                textDisc.setVisibility(View.VISIBLE);
            } else {
                disponibilitats = user.getDisponibilitats();
                if (disponibilitats != null) {
                    String text = "";
                    for (int i = 0; i < disponibilitats.size(); i++) {
                        DisponibilitatDia dispDia = disponibilitats.get(i);
                        if (i == 0) {
                            text += dispDia.getNomDia() + " " + getResources().getString(R.string.de) + " " + dispDia.gethIni() +  " " + getResources().getString(R.string.a) + " " +  dispDia.gethFi();
                        } else {
                            text += ", " + dispDia.getNomDia() + " "  + getResources().getString(R.string.de) + " " + dispDia.gethIni()  +  " " + getResources().getString(R.string.a) + " " + dispDia.gethFi();
                        }
                    }
                    textDisp.setText(text);
                    textDisp.setVisibility(View.VISIBLE);
                }
            }
        } else {
            Intent intent = new Intent(this, PantallaLogin.class);
            startActivity(intent);
            finish();
        }
    }

    private void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.perfil));

    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Botó Editar a la Toolbar
        if (id == R.id.action_edit) {

            inTelefon.setEnabled(true);
            inTelefon.setVisibility(View.VISIBLE);
            inUsuari.setEnabled(true);
            inMail.setEnabled(true);
            inDiscapacitat.setEnabled(true);
            inBirthday.setEnabled(true);
            inBirthday.setVisibility(View.VISIBLE);
            inPasswordAntiga.setEnabled(true);
            inPassword.setEnabled(true);
            inPasswordConfirm.setEnabled(true);
            inNom.setEnabled(true);

            textSexe.setVisibility(View.VISIBLE);
            viewGender.setVisibility(View.GONE);
            spinnerGender.setVisibility(View.VISIBLE);

            textModificarPassword.setVisibility(View.VISIBLE);
            inPasswordAntiga.setVisibility(View.VISIBLE);
            inPassword.setVisibility(View.VISIBLE);
            inPasswordConfirm.setVisibility(View.VISIBLE);

            buttonEditarPhoto.setVisibility(View.VISIBLE);
            buttonEditar.setVisibility(View.VISIBLE);

            if (user.getRol().equals("Ajudant")) {
                spinnerDisponibilitat.setVisibility(View.VISIBLE);
            }else {
                textDisc.setVisibility(View.VISIBLE);
            }


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Funcio per ensenyar el DataPicker
    private void showDatePickerDialog(final EditText editText) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = twoDigits(day) + "/" + twoDigits(month+1) + "/" + year;
                editText.setText(selectedDate);
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    public void onActivityResult(int requestCode,int resultCode,Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == 1) { //data.getData returns the content URI for the selected Image
                selectedImage = data.getData();
                profilePic.setImageURI(selectedImage);
            }
    }


}
