package com.example.aidbee;

public class DisponibilitatDia {
    private String nomDia;
    private String hIni;
    private String hFi;

    public DisponibilitatDia(String nomDia, String hIni, String hFi) {
        this.nomDia = nomDia;
        this.hIni = hIni;
        this.hFi = hFi;
    }

    public String getNomDia() {
        return nomDia;
    }

    public void setNomDia(String nomDia) {
        this.nomDia = nomDia;
    }

    public String gethIni() {
        return hIni;
    }

    public void sethIni(String hIni) {
        this.hIni = hIni;
    }

    public String gethFi() {
        return hFi;
    }

    public void sethFi(String hFi) {
        this.hFi = hFi;
    }
}
