package com.example.aidbee;

import java.util.ArrayList;

public class Usuari {
    private String email;
    private String usuari;
    private String nom;
    private String aniversari;
    private String password;
    private String telefon;
    private String sexe;
    private String urlImg;
    private String discapacitat;
    private ArrayList<DisponibilitatDia> disponibilitats;
    private String rol; //Rol pot ser "UPM" o "Ajudant"
    private String device_token;
    private String upvotes;
    private String downvotes;


    public Usuari(){
        email = null;
        usuari = null;
        nom = null;
        aniversari = null;
        password = null;
        telefon = null;
        sexe = null;
        urlImg = null;
        discapacitat = null;
        disponibilitats = null;
        rol = null;
        device_token = null;
        upvotes = "0";
        downvotes = "0";
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(String upvotes) {
        this.upvotes = upvotes;
    }

    public String getDownvotes() { return downvotes; }

    public void setDownvotes(String downvotes) {
        this.downvotes = downvotes;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getAniversari() {
        return aniversari;
    }

    public void setAniversari(String aniversari) {
        this.aniversari = aniversari;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuari() {
        return usuari;
    }

    public void setUsuari(String usuari) {
        this.usuari = usuari;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String typeUser() {return ""; }

    public void setExtraInfo(String d, ArrayList<DisponibilitatDia> disp) {}

    public String getDiscapacitat() {return discapacitat;}

    public ArrayList<DisponibilitatDia> getDisponibilitats() {
        return disponibilitats;
    }

    public void setDiscapacitat(String discapacitat) {
        this.discapacitat = discapacitat;
    }

    public void setDisponibilitats(ArrayList<DisponibilitatDia> disponibilitats) {
        this.disponibilitats = disponibilitats;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

}

