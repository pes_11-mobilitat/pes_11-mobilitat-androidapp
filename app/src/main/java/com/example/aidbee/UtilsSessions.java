package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UtilsSessions {


    public static int registreUsuari(Usuari user) {
        AsyncStatus asyncStatus = new AsyncStatus();
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonUser = mapper.writeValueAsString(user);
            asyncStatus.execute("register", jsonUser, "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc");
            try {
                Integer status = asyncStatus.get();
                if (status.equals(400)) { //email ja agafat
                    return (-1);
                } else {
                    Context context = SplashActivity.getContext();
                    SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                    String userId = sharedPreferences.getString("userId", "0");
                    String tokenId = sharedPreferences.getString("idToken", "0");
                    AsyncStatus asyncStatus2 = new AsyncStatus();
                    asyncStatus2.execute("newuser", userId, jsonUser, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/NewUser");
                    return (0); //OK.
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return (-2); //no hauria d'arribar aquí
    }

    public static int editUsuari(Usuari user) {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonUser = mapper.writeValueAsString(user);
            asyncStatus.execute("edituser", userId, jsonUser, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/EditUser");
            try {
                Integer status = asyncStatus.get();
                if (status.equals(403)) {
                    status = refreshToken();
                    if (status.equals(403)) {
                        return (-1); //tornar a fer login (idtoken i refreshToken des-actualitzats)
                    } else {
                        AsyncStatus asyncStatus1 = new AsyncStatus();
                        asyncStatus1.execute("edituser", userId, jsonUser, "https://us-central1-pes11-mobilitat.cloudfunctions.net/EditUser");
                        return 0; //OK, s'actualitza idtoken mitjançant refreshtoken
                    }
                } else {
                    return 0; //OK, idtoken encara està actualitzat
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return (-2); //no hauria d'arribar aquí
    }

    public static int eliminarUsuari(String pass){
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String email = sharedPreferences.getString("email", "0");
        String idToken = sharedPreferences.getString("idToken", "0");
        asyncStatus.execute("login", email, pass, "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc");
        try {
            Integer status = asyncStatus.get();
            if (status.equals(400)) {
                return(-1); //password incorrecte
            }
            else {
                AsyncStatus asyncStatus1 = new AsyncStatus();
                asyncStatus1.execute("deleteuser", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteUser");
                try {
                    status = asyncStatus1.get();
                    if (status.equals(403)) { //
                        status = refreshToken();
                        if (status.equals(403)) {
                            return (-2); //tokens caducats (no hauria de passar mai perque acabo de fer login
                        } else {
                            AsyncStatus asyncStatus2 = new AsyncStatus();
                            asyncStatus2.execute("deleteuser", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteUser");
                            return (0);
                        }
                    } else return (0);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (-3); //No arriba aquí (en principi)
    }

    public static int loginUsuari(String email, String pass){
        AsyncStatus asyncStatus = new AsyncStatus();
        asyncStatus.execute("login", email, pass, "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc");
        try {
            Integer status = asyncStatus.get();
            if (status.equals(400)) {
                return(-1); //mail o password incorrecte
            }
            else {
                return (0); //OK
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -2; //No arriba aquí (en principi)
    }

    public static void logoutUsuari(){
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String idToken = sharedPreferences.getString("idToken", "0");
        asyncStatus.execute("logout", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/LogoutUser");
        try {
            Integer status = asyncStatus.get();
            if (status.equals(403)) { //
                status = refreshToken();
                if (status.equals(403)) {
                    return;
                     //tokens caducats
                } else {
                    AsyncStatus asyncStatus1 = new AsyncStatus();
                    asyncStatus1.execute("logout", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/LogoutUser");
                    return;
                }
            } else return;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Usuari getUser() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String idToken = sharedPreferences.getString("idToken", "0");
        String stringUser;
        Integer status;
        asyncMessage.execute("getuser", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetUser");
        try {
            stringUser = asyncMessage.get();
            if (stringUser.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("getuser", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetUser");
                    try {
                        stringUser = asyncMessage1.get();
                        ObjectMapper mapper = new ObjectMapper();
                        Usuari user = mapper.readValue(stringUser, Usuari.class);
                        return user;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ObjectMapper mapper = new ObjectMapper();
                Usuari user = mapper.readValue(stringUser, Usuari.class);
                return user;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        Usuari user = null;
        return user; //no hauria d'arribar mai aqui
    }

    public static Usuari getUser(String id) throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String stringUser;
        Integer status;
        asyncMessage.execute("getuser", id, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetUser");
        try {
            stringUser = asyncMessage.get();
            if (stringUser.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("getuser", id, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetUser");
                    try {
                        stringUser = asyncMessage1.get();
                        ObjectMapper mapper = new ObjectMapper();
                        Usuari user = mapper.readValue(stringUser, Usuari.class);
                        return user;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ObjectMapper mapper = new ObjectMapper();
                Usuari user = mapper.readValue(stringUser, Usuari.class);
                return user;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        Usuari user = null;
        return user; //no hauria d'arribar mai aqui
    }

    public static Integer refreshToken() {
        Integer status = null;
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String refreshToken = sharedPreferences.getString("refreshToken", "0");
        asyncStatus.execute("refreshToken", refreshToken, "https://securetoken.googleapis.com/v1/token?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc");
        try {
            status = asyncStatus.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public static void setDeviceToken(String deviceToken) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String idToken = sharedPreferences.getString("idToken", "0");
        asyncStatus.execute("setdevicetoken", userId, deviceToken, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/SetTokenDevice");
        try {
            Integer status = asyncStatus.get();
            if (status.equals(403)) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                    //tokens caducats
                } else {
                    AsyncStatus asyncStatus1 = new AsyncStatus();
                    asyncStatus1.execute("setdevicetoken", userId, deviceToken, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/SetTokenDevice");
                    return;
                }
            } else return;
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
