package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import static com.example.aidbee.UtilsSessions.refreshToken;


public class UtilsUbicacio {
    public static void pushUbicacio(LatLng ubi) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String latitud = String.valueOf(ubi.latitude);
        String longitud = String.valueOf(ubi.longitude);
        asyncStatus.execute("updateubicacio", userId, tokenId, latitud, longitud, "https://us-central1-pes11-mobilitat.cloudfunctions.net/UpdateLocation");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("updateubicacio", userId, tokenId, latitud, longitud, "https://us-central1-pes11-mobilitat.cloudfunctions.net/UpdateLocation");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public static LatLng getUbicacio(String userId) throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String idToken = sharedPreferences.getString("idToken", "0");
        String stringUbi;
        Integer status;
        asyncMessage.execute("getubicacio", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetLocation");
        try {
            stringUbi = asyncMessage.get();
            if (stringUbi.equals("ERROR")) { //
                status = refreshToken();
                if (status.equals(403)) {
                    throw new AuthenticatorException();
                } else {
                    AsyncMessage asyncMessage1 = new AsyncMessage();
                    asyncMessage1.execute("getubicacio", userId, idToken, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetLocation");
                    try {
                        stringUbi = asyncMessage1.get();
                        JSONObject jsonUbi = new JSONObject(stringUbi);
                        String lat = jsonUbi.getString("latitude");
                        String lon = jsonUbi.getString("longitude");
                        LatLng ubi = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                        return ubi;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                JSONObject jsonUbi = new JSONObject(stringUbi);
                String lat = jsonUbi.getString("latitude");
                String lon = jsonUbi.getString("longitude");
                LatLng ubi = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                return ubi;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        LatLng ubi= null;
        return ubi; //no hauria d'arribar mai aqui
    }

}


