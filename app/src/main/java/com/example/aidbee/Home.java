package com.example.aidbee;

import android.Manifest;
import android.accounts.AuthenticatorException;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.example.aidbee.NotificationListener.updateStatusFragment;
import static com.example.aidbee.UtilsSessions.getUser;
import static com.example.aidbee.ui.home.HomeFragment.setGoneVisibilityBuscarButton;
import static com.example.aidbee.utilsMatch.getIdVoluntariMatch;

public class Home extends AppCompatActivity {

    public static String nomUserMatch;
    public static Usuari user;
    private AppBarConfiguration mAppBarConfiguration;
    private FloatingActionButton fab;
    public static String SHOW_LOGGED_OUT_EXTRA = "SHOW_LOGGED_OUT";
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static Location actualLocation;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateStatusFragment(false);
        setContentView(R.layout.activity_home);
        try {
            user = getUser();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        //Demana permisos xat
        if (!NotificationManagerCompat.getEnabledListenerPackages(this).contains(getPackageName())) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Activar Permisos");
            // R.string.app_name
            builder.setMessage("Cal activar els permisos següents per actualitzar el xat a temps real.");
            builder.setPositiveButton("Acceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    startActivity(intent);
                    dialog.dismiss();
                }

            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }

            });
            android.app.AlertDialog alert = builder.create();
            alert.show();
        }
        String rol = user.getRol();
        if (FactoryStatusApplication.getInstance().getFirstTimeConnected()) {
            if (rol.equals("UPM"))
                FactoryStatusApplication.getInstance().createStatusApplication(0);
            else FactoryStatusApplication.getInstance().createStatusApplication(1);
        } else {
            if (FactoryStatusApplication.getInstance().getStatusApplication().getRefreshMessage()) {
                FactoryStatusApplication.getInstance().getStatusApplication().setRefreshMessage(false);
                FragmentManager fm = getSupportFragmentManager();
                XatFragment xatFragment = new XatFragment();
                fm.beginTransaction().replace(R.id.home_layout, xatFragment).commit();
            } else {
                if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() == 3) {
                    //acció definida en el home fragment
                } else if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() == 2) {
                    // desactivar boto
                    setGoneVisibilityBuscarButton();
                    FragmentManager fm = getSupportFragmentManager();
                    MapsFragment mapsFragment = new MapsFragment();
                    fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
                } else if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() == 7) {
                    Intent intent = new Intent(Home.this, PantallaValoracioAjudant.class);
                    startActivity(intent);
                }
            }
        }

        checkLocationPermission();

        //Get Current Location
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            //ES NECESSARI ACTIVAR ELS PERMISOS DE UBICACIO!!!!
                            LatLng myLoc = new LatLng(location.getLatitude(), location.getLongitude());
                            try {
                                UtilsUbicacio.pushUbicacio(myLoc);
                            } catch (AuthenticatorException e) {
                                e.printStackTrace();
                            }
                            setActualLocation(location);
                        }
                    }
                });

        //canal de notificacions

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("MyNotifications", "MyNotifications", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);

            manager.createNotificationChannel(channel);
        }

        try {
            if (rol.equals("UPM")) {
                String userId = getIdVoluntariMatch();
                if (userId != null) {
                    FragmentManager fm = getSupportFragmentManager();
                    MapsFragment mapsFragment = new MapsFragment();
                    fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
                }
            }
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }


        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Floating button
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                XatFragment xatFragment = new XatFragment();
                fm.beginTransaction().replace(R.id.home_layout, xatFragment).commit();

            }
        });
        // Navigation

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        //navegació
        Menu menuNav = navigationView.getMenu();
        MenuItem navitem2 = menuNav.findItem(R.id.nav_valoracions);
        MenuItem navitem3 = menuNav.findItem(R.id.nav_events);
        if (rol.equals("UPM")) {
            navitem2.setVisible(false);
            navitem3.setVisible(false);
        }

        //Imatge Navigation
        View headerView = navigationView.getHeaderView(0);
        ImageView imgProfile = headerView.findViewById(R.id.imageView);
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, PerfilUsuari.class);
                startActivity(intent);
            }
        });

        TextView tv_name = headerView.findViewById(R.id.textViewName);
        TextView tv_email = headerView.findViewById(R.id.textViewEmail);
        String Img = user.getUrlImg();
        if (Img != null) {
            String url = user.getUrlImg();
            Glide.with(this)
                    .load(url)
                    .fitCenter()
                    .centerCrop()
                    .into(imgProfile);
        }
        tv_name.setText(user.getNom());
        tv_email.setText(user.getEmail());



        /*Usuari user = null;
        try {
            user = getUser();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        Uri selectedImage = Uri.parse(user.getUrlImg());
        imgProfile.setImageURI(selectedImage);*/

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_events,R.id.nav_my_events,R.id.nav_history)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                switch (destination.getId()) {
                    /*case R.id.nav_events:
                        fab.show();
                        break;
                    /*case R.id.nav_home:
                        fab.show();
                        break;*/
                    default:
                        fab.hide();
                        break;
                }
            }
        });

        // Handle logout
        LinearLayout logoutButton = (LinearLayout) findViewById(R.id.logout_button);
        logoutButton.setClickable(true);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        //getSupportFragmentManager().beginTransaction().add(R.id_fragment_container,new FavouriteListFragment()).commit();

        FirebaseMessaging.getInstance().subscribeToTopic("general")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Exit";
                        if (!task.isSuccessful()) {
                            msg = "Error";
                        }
                        //Toast.makeText(Home.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        UtilsSessions.logoutUsuari();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        Intent i = new Intent(Home.this, PantallaLogin.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra(SHOW_LOGGED_OUT_EXTRA, true);
        startActivity(i);
        finish();
    }

    static public Location getActualLocation() {
        return actualLocation;
    }

    public void setActualLocation(Location actualLocation) {
        this.actualLocation = actualLocation;
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    public void checkLocationPermission() {
        if(Build.VERSION.SDK_INT >=23)

        {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                                android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }

        getLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Activa permisos d'ubicació", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //Get location
    public void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        Location myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null)
        {
            myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        }
    }
}
