package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.json.JSONObject;

import java.io.DataInput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.aidbee.UtilsSessions.refreshToken;

public class UtilsXat {

    public static ArrayList<ChatMessage> getMessages(String chatId) throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String messages;
        ArrayList<ChatMessage> arrayMsg = new ArrayList<ChatMessage>();
        asyncMessage.execute("getMessages", userId, chatId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetChatMessages");
        try {
            messages = asyncMessage.get();
            if (messages.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("getMessages", userId, chatId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetChatMessages");
                try {
                    messages = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONObject jObj = new JSONObject(messages.trim());
                    Iterator<String> keys = jObj.keys();
                    while(keys.hasNext()) {
                        String key = keys.next();
                        if (jObj.get(key) instanceof JSONObject) {
                            Object jsonmsg = jObj.get(key);
                            ChatMessage msg = mapper.readValue(jsonmsg.toString(), ChatMessage.class);
                            arrayMsg.add(msg);
                        }
                    }
                    return arrayMsg;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    messages = asyncMessage.get();
                    ObjectMapper mapper = new ObjectMapper();
                    JSONObject jObj = new JSONObject(messages.trim());
                    Iterator<String> keys = jObj.keys();
                    while(keys.hasNext()) {
                        String key = keys.next();
                        if (jObj.get(key) instanceof JSONObject) {
                            Object jsonmsg = jObj.get(key);
                            ChatMessage msg = mapper.readValue(jsonmsg.toString(), ChatMessage.class);
                            arrayMsg.add(msg);
                        }
                    }
                    return arrayMsg;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayMsg;
    }

    public static String getChatId() throws AuthenticatorException {
        AsyncMessage asyncMessage = new AsyncMessage();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        String messages;
        String chatId = new String();
        asyncMessage.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
        try {
            messages = asyncMessage.get();
            if (messages.equals("ERROR")) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncMessage asyncMessage2 = new AsyncMessage();
                asyncMessage2.execute("getmatch", userId, tokenId, "https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch");
                try {
                    messages = asyncMessage2.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    chatId = jsonMsg.getString("chatroom_id");
                    return chatId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    messages = asyncMessage.get();
                    JSONObject jsonMsg = new JSONObject(messages);
                    chatId = jsonMsg.getString("chatroom_id");
                    return chatId;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (chatId);
    }

    public static void sendMessage(String chatId, String msg) throws AuthenticatorException {
        AsyncStatus asyncStatus = new AsyncStatus();
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");
        String tokenId = sharedPreferences.getString("idToken", "0");
        Integer status;
        asyncStatus.execute("sendMessage", userId, chatId, tokenId, msg, "https://us-central1-pes11-mobilitat.cloudfunctions.net/SendMessage");
        try {
            status = asyncStatus.get();
            if (status.equals(403)) {
                status = refreshToken();
                if (status == 403) {
                    throw new AuthenticatorException();
                }
                AsyncStatus asyncStatus2 = new AsyncStatus();
                asyncStatus2.execute("sendMessage", userId, chatId, tokenId, msg, "https://us-central1-pes11-mobilitat.cloudfunctions.net/SendMessage");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

}