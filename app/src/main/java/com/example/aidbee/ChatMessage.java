package com.example.aidbee;

import java.util.Date;

public class ChatMessage implements Comparable{
    private String message;
    private String sender;
    private long messagetime;

    public ChatMessage(String message, String sender) {
        this.message = message;
        this.sender = sender;

        // Initialize to current time
        messagetime = new Date().getTime();
    }

    public ChatMessage(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getMessagetime() {
        return messagetime;
    }

    public void setMessagetime(long messagetime) {
        this.messagetime = messagetime;
    }

    @Override
    public int compareTo(Object o) {
        long compareTime=((ChatMessage)o).getMessagetime();
        /* For Ascending order*/
        return (int) (this.messagetime-compareTime);

        /* For Descending order do like this */
        //return compareage-this.studentage;
    }
}
