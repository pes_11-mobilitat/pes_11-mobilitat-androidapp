package com.example.aidbee;

public class StateVO {
    private String title;
    private boolean selected;
    private String hIni;
    private String hFi;

    public String gethIni() {
        return hIni;
    }

    public void sethIni(String hIni) {
        this.hIni = hIni;
    }

    public String gethFi() {
        return hFi;
    }

    public void sethFi(String hFi) {
        this.hFi = hFi;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}