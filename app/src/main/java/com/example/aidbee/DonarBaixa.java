package com.example.aidbee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.example.aidbee.UtilsSessions.eliminarUsuari;

public class DonarBaixa extends AppCompatActivity {
    Button buttonAccept;
    EditText inPassword, inPasswordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donar_baixa);

        Toolbar toolbar = findViewById(R.id.donarbaixaToolbar);
        initToolbar(toolbar);

        buttonAccept = findViewById(R.id.buttonAccept);

        inPassword = findViewById(R.id.inPassword);
        inPasswordConfirm = findViewById(R.id.inPasswordConfirm);


        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String pass = String.valueOf(inPassword.getText());
                String Confirm_pass = String.valueOf(inPasswordConfirm.getText());
                if (pass.isEmpty()) {
                    inPassword.setError(getResources().getString(R.string.indica_contrasenya));
                    inPassword.requestFocus();
                    return;
                }
                else if (Confirm_pass.isEmpty()) {
                    inPasswordConfirm.setError(getResources().getString(R.string.confirma_password));
                    inPasswordConfirm.requestFocus();
                    return;

                }
                else if (!pass.equals(Confirm_pass)){
                    inPasswordConfirm.setError(getResources().getString(R.string.no_coincideix));
                    inPasswordConfirm.requestFocus();
                    return;

                }
                else {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(DonarBaixa.this);
                    builder.setTitle(getResources().getString(R.string.donar_de_baixa));
                    // R.string.app_name
                    builder.setMessage(getResources().getString(R.string.confirma_esborrar_compte));
                    builder.setPositiveButton(getResources().getString(R.string.acceptar), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            eliminarUsuari(pass);
                            Intent intent = new Intent(DonarBaixa.this, Home.class);
                            startActivity(intent);
                        }

                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }

                    });
                    android.app.AlertDialog alert = builder.create();
                    alert.show();

                }
            }
        });
    }

    private void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.registre));
        //Personalitzar buttton back: https://stackoverflow.com/questions/26651602/display-back-arrow-on-toolbar
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
