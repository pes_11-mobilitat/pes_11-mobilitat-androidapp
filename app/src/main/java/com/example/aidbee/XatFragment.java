package com.example.aidbee;



import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;


import com.google.android.gms.common.internal.Constants;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.protobuf.StringValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.example.aidbee.UtilsEsdeveniments.getEventPrivate;
import static com.example.aidbee.UtilsXat.getChatId;
import static com.example.aidbee.UtilsXat.getMessages;
import static com.example.aidbee.UtilsXat.sendMessage;


public class XatFragment extends Fragment {

    private static MessageAdapter adapter;
    private static ListView listOfMessages;
    private static String chatId;
    public static String userId;

    public static void refreshChatMessages() {

        ArrayList<ChatMessage> missatges = null;
        try {
            missatges = getMessages(chatId);
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }


        adapter.deleteMessages();
        for(int i=0;i<missatges.size();i++) {
            ChatMessage missatge = missatges.get(i);
            adapter.add(missatge);
        }

        adapter.notifyDataSetChanged();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotificationListener.updateStatusFragment(true);
        try {
            chatId = getChatId();
            if((chatId.length()) == 0){
                chatId = VisualitzarActivitatPlanejadaMaps.esdeveniment.getCid();
            }
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }

        //obtenir id propi
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "0");

        /*FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mReference = database.getReference().child("chatroom").child(String.valueOf(chatId));
        mReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d("CHAT","SUCCESS!");
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("CHAT","ERROR: " + databaseError.getMessage());
            }
        });
         */
    }

    private void displayChatMessages() throws AuthenticatorException {

        //OBTENIR MISSATGES
        ArrayList<ChatMessage> missatges = getMessages(chatId);

        Collections.sort(missatges);

        /*ChatMessage prova = new ChatMessage();
        prova.setMessage("loooooooool");
        prova.setSender("Pau Ballber");
        missatges.add(prova);

        ChatMessage prova2 = new ChatMessage();
        prova2.setMessage("nooooo");
        prova2.setSender("real");
        missatges.add(prova2);
         */

        adapter = new MessageAdapter(getContext());

        for(int i=0;i<missatges.size();i++) {
            ChatMessage missatge = missatges.get(i);
            adapter.add(missatge);
        }
        listOfMessages.setAdapter(adapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_xat, container, false);


        listOfMessages = v.findViewById(R.id.messages_view);

        try {
            displayChatMessages();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }

        ImageButton send = v.findViewById(R.id.sendText);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = v.findViewById(R.id.editText);
                String missatge = String.valueOf(input.getText());

                if (!missatge.equals("")) {
                    String userId = SplashActivity.getContext().getSharedPreferences("tokens", Context.MODE_PRIVATE).getString("userId", "0");
                    ChatMessage nou_missatge = new ChatMessage(missatge, userId);
                    adapter.add(nou_missatge);
                    adapter.notifyDataSetChanged();
                    //Enviar missatge BACKEND
                    try {
                        sendMessage(chatId, missatge);
                    } catch (AuthenticatorException e) {
                        e.printStackTrace();
                    }
                    // Clear the input
                    input.setText("");
                }
            }
        });

        return v;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        NotificationListener.updateStatusFragment(false);
    }

    @Override
    public void onDestroy() {
        NotificationListener.updateStatusFragment(false);
        super.onDestroy();
    }

}
