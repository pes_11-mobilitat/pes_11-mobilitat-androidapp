package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import static com.example.aidbee.UtilsSessions.registreUsuari;
import static com.example.aidbee.UtilsSessions.setDeviceToken;


public class RegistreUsuari extends AppCompatActivity {
    Button buttonRegister;
    EditText inEmail, inUsuari, inPassword, inPasswordConfirm, inDisc, inNom;
    RadioButton inAjudant, inUPM;
    Spinner spinner;
    String[] select_days;
    ArrayList<StateVO> listVOs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.select_days = new String[]{getResources().getString(R.string.availability), getResources().getString(R.string.dilluns), getResources().getString(R.string.dimarts), getResources().getString(R.string.dimecres), getResources().getString(R.string.dijous), getResources().getString(R.string.divendres), getResources().getString(R.string.dissabte), getResources().getString(R.string.diumenge)};

        Toolbar toolbar = findViewById(R.id.toolbar);
        initToolbar(toolbar);
        buttonRegister = findViewById(R.id.buttonRegister);
        inEmail = findViewById(R.id.inEmail);
        inUsuari = findViewById(R.id.inUsuari);
        inPassword = findViewById(R.id.inPassword);
        inPasswordConfirm = findViewById(R.id.inPasswordConfirm);
        inAjudant = findViewById(R.id.inAjudant);
        inUPM = findViewById(R.id.inUPM);
        spinner = findViewById(R.id.spinner);
        inDisc = findViewById(R.id.inDisc);
        inNom = findViewById(R.id.inNom);
        listVOs = new ArrayList<>();
        inDisc.setVisibility(View.GONE);


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = String.valueOf(inEmail.getText());
                String nomUser = String.valueOf(inUsuari.getText());
                String pass = String.valueOf(inPassword.getText());
                String passConfirm = String.valueOf(inPasswordConfirm.getText());
                String disc = null;
                String nom = String.valueOf(inNom.getText());
                String rol = null;
                boolean UMP = inUPM.isChecked();
                if (email.isEmpty()) {
                    inEmail.setError(getResources().getString(R.string.indica_mail));
                    inEmail.requestFocus();
                    return;
                }
                else if (nomUser.isEmpty()) {
                    inUsuari.setError(getResources().getString(R.string.indica_usuari));
                    inUsuari.requestFocus();
                    return;
                }
                else if (pass.isEmpty() || pass.length() < 6) {
                    inPassword.setError(getResources().getString(R.string.indica_contrasenya));
                    inPassword.requestFocus();
                    return;
                }
                else if (passConfirm.isEmpty()) {
                    inPasswordConfirm.setError(getResources().getString(R.string.confirma_password));
                    inPasswordConfirm.requestFocus();
                    return;
                }
                else if (!pass.equals(passConfirm)) {
                    inPasswordConfirm.setError(getResources().getString(R.string.no_coincideix));
                    inPasswordConfirm.requestFocus();
                    return;
                }
                else if (nom.isEmpty()) {
                    inNom.setError(getResources().getString(R.string.nom_complet));
                    inNom.requestFocus();
                }
                else {
                    /* REGISTRAR USUARI */
                    Usuari user = new Usuari();
                    ArrayList<DisponibilitatDia> disponibilitats = null;
                    if (UMP) {
                        rol = "UPM";
                        disc = String.valueOf(inDisc.getText());
                        if (disc.isEmpty()) {
                            inDisc.setError(getResources().getString(R.string.indica_discapacitat));
                            inDisc.requestFocus();
                        }
                    }
                    else {
                        rol = "Ajudant";
                        disponibilitats = new ArrayList<>();
                        for (int i = 1; i < listVOs.size(); i++) {
                            if (listVOs.get(i).isSelected()) {
                                DisponibilitatDia aux = new DisponibilitatDia(listVOs.get(i).getTitle(),
                                        listVOs.get(i).gethIni(),
                                        listVOs.get(i).gethFi());
                                disponibilitats.add(aux);
                            }
                        }
                    }
                    user.setEmail(email);
                    user.setNom(nom);
                    user.setPassword(pass);
                    user.setUsuari(nomUser);
                    user.setDiscapacitat(disc);
                    user.setDisponibilitats(disponibilitats);
                    user.setRol(rol);
                    //Comprovar horaris correctes!!!
                    int code = registreUsuari(user);
                    if (code == 0) {
                        Toast toast1 =
                                Toast.makeText(getApplicationContext(),
                                        getResources().getString(R.string.registrat_ok), Toast.LENGTH_SHORT);

                        toast1.show();
                        FirebaseInstanceId.getInstance().getInstanceId()
                                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                        if (!task.isSuccessful()) {
                                            return;
                                        }

                                        // Get new Instance ID token
                                        String deviceToken = task.getResult().getToken();
                                        try {
                                            setDeviceToken(deviceToken);
                                        } catch (AuthenticatorException e) {
                                            e.printStackTrace(); //GO LOGIN
                                        }
                                    }
                                });

                        Intent intent = new Intent(RegistreUsuari.this, Home.class);
                        startActivity(intent);
                    }
                }
            }
        });

        //Init spinnerDisponibilitat adapter
        for (int i = 0; i < select_days.length; i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(select_days[i]);
            stateVO.setSelected(false);
            stateVO.sethFi("00");
            stateVO.sethFi("00");
            listVOs.add(stateVO);
        }
        MyDaysSpinnerAdapter myAdapter = new MyDaysSpinnerAdapter(RegistreUsuari.this, 0, listVOs);
        spinner.setAdapter(myAdapter);

        //Listeners
        inUPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setVisibility(View.GONE);
                inDisc.setVisibility(View.VISIBLE);
            }
        });

        inAjudant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setVisibility(View.VISIBLE);
                inDisc.setVisibility(View.GONE);
            }
        });
    }

    private void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.registre));

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
        });*/
        //Personalitzar buttton back: https://stackoverflow.com/questions/26651602/display-back-arrow-on-toolbar
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
