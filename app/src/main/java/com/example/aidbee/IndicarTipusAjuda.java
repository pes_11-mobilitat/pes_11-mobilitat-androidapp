package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.aidbee.ui.home.HomeFragment;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

import static com.example.aidbee.utilsMatch.newHelpRequest;
import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

/**
 * A simple {@link Fragment} subclass.
 */
/*Aquesta classe nomes la executa el UPM*/
public class IndicarTipusAjuda extends Fragment {
    //supportAjudaFragment ajudaFragment;

    //private static AppCompatActivity instance;
    private boolean clicked1, clicked2, clicked3, clicked4;
    private String TipusAjuda;
    private int Duracio;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.indicartipusajuda, container, false);
        clicked1 = clicked2 = clicked3 = clicked4 = false;
        Duracio = 0;
        final ImageButton ferlacompra = v.findViewById(R.id.ferlacompra);
        ferlacompra.setBackgroundColor(Color.parseColor("#FAFAFA"));
        final ImageButton cadirarodes = v.findViewById(R.id.cadirarodes);
        cadirarodes.setBackgroundColor(Color.parseColor("#FAFAFA"));
        final ImageButton metro = v.findViewById(R.id.metro);
        metro.setBackgroundColor(Color.parseColor("#FAFAFA"));
        final ImageButton altres = v.findViewById(R.id.altres);
        altres.setBackgroundColor(Color.parseColor("#FAFAFA"));
        TipusAjuda = "";
        ferlacompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fm.beginTransaction().attach(ajudaFragment).commit();
                if(!clicked1){
                    ferlacompra.setBackgroundColor(Color.parseColor("#ECCD6A"));
                    clicked1 = true;
                    clicked2 = clicked3 = clicked4 = false;
                    cadirarodes.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    metro.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    altres.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    TipusAjuda = "Fer la compra";
                }
                else{
                    TipusAjuda = "";
                    clicked1 = false;
                    ferlacompra.setBackgroundColor(Color.parseColor("#FAFAFA"));
                }
                //fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
                return;
            }
            //return root;
        });
        metro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fm.beginTransaction().attach(ajudaFragment).commit();
                if(!clicked2){
                    metro.setBackgroundResource(R.color.colorPrimary);
                    clicked2 = true;
                    clicked1 = clicked3 = clicked4 = false;
                    cadirarodes.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    ferlacompra.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    altres.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    TipusAjuda = "Anar en metro";
                }
                else{
                    TipusAjuda = "";
                    clicked2 = false;
                    metro.setBackgroundColor(Color.parseColor("#FAFAFA"));
                }
                //fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
            }
            //return root;
        });
        cadirarodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fm.beginTransaction().attach(ajudaFragment).commit();
                if(!clicked3){
                    cadirarodes.setBackgroundResource(R.color.colorPrimary);
                    clicked3 = true;
                    clicked2 = clicked1 = clicked4 = false;
                    ferlacompra.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    metro.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    altres.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    TipusAjuda = "Anar al metge";
                }
                else{
                    TipusAjuda = "";
                    clicked3 = false;
                    cadirarodes.setBackgroundColor(Color.parseColor("#FAFAFA"));
                }
                //fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
            }
            //return root;
        });
        altres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fm.beginTransaction().attach(ajudaFragment).commit();
                if(!clicked4){
                    altres.setBackgroundResource(R.color.colorPrimary);
                    clicked4 = true;
                    clicked2 = clicked1 = clicked3 = false;
                    cadirarodes.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    metro.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    ferlacompra.setBackgroundColor(Color.parseColor("#FAFAFA"));
                    TipusAjuda = "Altres";
                }
                else{
                    TipusAjuda = "";
                    clicked4 = false;
                    altres.setBackgroundColor(Color.parseColor("#FAFAFA"));
                }
                //fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
            }
            //return root;
        });
        Spinner spinner = v.findViewById(R.id.indicartemps);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("10 min");
        arrayList.add("30 min");
        arrayList.add("1 h");
        arrayList.add("+1 h");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String Temps = parent.getItemAtPosition(position).toString();
                if(Temps == "10 min") Duracio = 10;
                else if(Temps == "30 min") Duracio = 30;
                else if(Temps == "1 h") Duracio = 60;
                else Duracio = 61;
                //Toast.makeText(parent.getContext(), "Selected: " + Temps, Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
                Duracio = 0;
            }
        });

        final Button acceptar = v.findViewById(R.id.botoindicarajuda);
        acceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TipusAjuda == ""){
                    acceptar.setError(getResources().getString(R.string.select_t_ajuda));

                }
                else if(Duracio == 0){
                    acceptar.setError(getResources().getString(R.string.select_temps));
                }
                else{
                    Location location = Home.getActualLocation();
                    LatLng randomLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    try {
                        newHelpRequest(randomLocation,TipusAjuda,Duracio); //envia notificaio al ajudant!
                        acceptar.setVisibility(View.GONE);
                        FragmentManager fm = getFragmentManager();
                        HomeFragment homeFragment = new HomeFragment();
                        fm.beginTransaction().replace(R.id.home_layout, homeFragment).commit();
                    } catch (AuthenticatorException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return v;
    }

}
