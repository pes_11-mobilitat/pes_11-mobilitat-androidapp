package com.example.aidbee;

public class Esdeveniment {
    private String title;
    private Double lat;
    private Double lon;
    private Integer daytime;
    private String date;
    private String uid;
    private String volunteer;
    private String descripcio;
    private String id;
    private String cid;

    public Esdeveniment() {
        id = null;
        title = null;
        lat = null;
        lon = null;
        daytime = null;
        date = null;
        uid = null;
        descripcio = null;
        volunteer = null;
        cid = null;
    }

    public Esdeveniment(String title, Double lat, Double lon, Integer daytime, String date, String uid, String descripcio) {
        this.title = title;
        this.lat = lat;
        this.lon = lon;
        this.daytime = daytime;
        this.date = date;
        this.uid = uid;
        this.descripcio = descripcio;
        this.volunteer = null;
        this.id = null;
        this.cid = null;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getDaytime() {
        return daytime;
    }

    public void setDaytime(Integer daytime) {
        this.daytime = daytime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(String volunteer) {
        this.volunteer = volunteer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
}
