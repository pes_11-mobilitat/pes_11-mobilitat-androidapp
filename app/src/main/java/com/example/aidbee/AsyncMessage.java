package com.example.aidbee;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class AsyncMessage extends AsyncTask<String, Void, String>{
    @Override
    protected String doInBackground(String... string) {
        Integer status = null;
        StringBuffer response = new StringBuffer();
        if (string[0].equals("getuser")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("getubicacio")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());

                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }  else if (string[0].equals("getnearrequest")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("location_lat", string[3]);
                    jsonOut.put("location_lon", string[4]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[5]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("getcurrentrequest")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("getmatch")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("getMessages")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                    jsonOut.put("cid", string[2]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[3]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("geteventpublic")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("eventid", string[2]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[1]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("geteventprivate")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("eventid", string[3]);
                    jsonOut.put("uid", string[2]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[4]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[1]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("geteventsupm")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("geteventsvoluntari")) {
            try {
                JSONObject jsonOut = new JSONObject();
                try {
                    jsonOut.put("uid", string[1]);
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[3]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[2]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("geteventsfiltered")) {
            try {
                JSONObject dist = new JSONObject();
                try {
                    if (string[4] != null) dist.put("lat", Double.parseDouble(string[4]));
                    if (string[5] != null) dist.put("lon", Double.parseDouble(string[5]));
                    if (string[6] != "null") dist.put("radius", Double.parseDouble(string[6]));
                } catch (JSONException e) {}
                JSONObject jsonOut = new JSONObject();
                try {
                    if (string[2] != "null") jsonOut.put("date", string[2]);
                    if (string[3] != "null") jsonOut.put("daytime", Integer.parseInt(string[3]));
                    if (dist.has("lat") && dist.has("lon") && dist.has("radius")) jsonOut.put("dist", dist);
                    jsonOut.put("socuncamp", "inutil");
                } catch (JSONException e) {}
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[7]);
                    con = (HttpURLConnection)url.openConnection();
                    String jsonRegistre = jsonOut.toString();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setRequestProperty("Authorization", "Bearer " +string[1]);
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.connect();

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            con.getOutputStream ());
                    wr.writeBytes (jsonRegistre);
                    wr.flush ();
                    wr.close ();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (string[0].equals("getesports")) {
                HttpURLConnection con = null;
                try {
                    //Create connection
                    URL url = new URL(string[1]);
                    con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoInput(true);
                    con.connect();

                    Log.i("STATUS", String.valueOf(con.getResponseCode()));
                    Log.i("MSG" , con.getResponseMessage());
                    //Get Response
                    InputStream is;
                    status = con.getResponseCode();
                    if (status != HttpURLConnection.HTTP_OK)
                        is = con.getErrorStream();
                    else
                        is = con.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(con != null) {
                        con.disconnect();
                    }
                }
        }
        if (status != HttpURLConnection.HTTP_OK) return ("ERROR");
        else return response.toString();
    }
}
