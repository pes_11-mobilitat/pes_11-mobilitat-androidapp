package com.example.aidbee;

import android.app.Activity;
import android.content.Intent;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.lang.ref.WeakReference;

public class NotificationListener extends NotificationListenerService {


    /* CAL ACTIVAR PERMISOS DE NOTIFICACIÓ
        if (!NotificationManagerCompat.getEnabledListenerPackages(this).contains(getPackageName())) {
            Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent);
        }
     */
    private static boolean estic_a_xat;
    public static void updateStatusFragment(boolean b) {
        estic_a_xat = b;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        // Implement what you want here
        String packageName = sbn.getPackageName();
        if (packageName.equals("com.example.aidbee_register")) {
            String title = sbn.getNotification().extras.getString("android.title");
            if (title.equals("Tens un missatge nou del teu Match!")) {
                boolean b = estic_a_xat;
                if (estic_a_xat) {
                    XatFragment.refreshChatMessages();
                    System.out.println("bé");
                }
            }
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        // Implement what you want here
    }
}
