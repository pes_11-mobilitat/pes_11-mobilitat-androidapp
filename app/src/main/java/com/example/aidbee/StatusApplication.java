package com.example.aidbee;

public class StatusApplication {

    private Integer statusCode;
    private Boolean refreshMessage;

    public StatusApplication(Integer code) {
        refreshMessage = false;
        statusCode = code;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getRefreshMessage() {
        return refreshMessage;
    }

    public void setRefreshMessage(Boolean refreshMessage) {
        this.refreshMessage = refreshMessage;
    }
}
