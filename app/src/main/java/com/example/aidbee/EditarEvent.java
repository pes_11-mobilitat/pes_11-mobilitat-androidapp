package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.aidbee.ui.home.HomeFragment;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.ArrayList;
import java.util.Arrays;

import static com.example.aidbee.UtilsEsdeveniments.editEvent;
import static com.example.aidbee.utilsMatch.newHelpRequest;
import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditarEvent extends Fragment{

    private int Duracio;
    private Esdeveniment esdeveniment;

    PlacesClient placesClient;
    private String apiKey = "AIzaSyCpZN5axCLyVw98IAmhpuIc5vnoihG4nmk";
    private LatLng localitzacio;
    private AutocompleteSupportFragment autocompleteSupportFragment;

    public EditarEvent(Esdeveniment esd) {
        this.esdeveniment = esd;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.crear_event, container, false);

        localitzacio = null;

        Duracio = 0;
        Spinner spinner = v.findViewById(R.id.indicartemps2);
        final EditText TitolEvent = v.findViewById(R.id.TitolEvent);
        TitolEvent.setText(esdeveniment.getTitle(), TextView.BufferType.EDITABLE);
        final EditText DescripcioEvent = v.findViewById(R.id.DescripcióEvent);
        DescripcioEvent.setText(esdeveniment.getDescripcio(), TextView.BufferType.EDITABLE);
        final EditText indicarData = v.findViewById(R.id.indicaData);
        indicarData.setText(esdeveniment.getDate(), TextView.BufferType.EDITABLE);
        indicarData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        final String Dataseleccionada = dayOfMonth + "-" + (month+1) + "-" + year;
                        indicarData.setText(Dataseleccionada);
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

            }
        });

        initSearchPlaces(v);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(getResources().getString(R.string.mati));
        arrayList.add(getResources().getString(R.string.tarda));
        arrayList.add(getResources().getString(R.string.totdia));

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        String DayTime;
        if(esdeveniment.getDaytime() == 1) DayTime = "Matí";
        else if (esdeveniment.getDaytime() == 2) DayTime = "Tarda";
        else DayTime = "Tot el dia";

        spinner.setSelection(getIndex(spinner, DayTime));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String Temps = parent.getItemAtPosition(position).toString();
                if(Temps == getResources().getString(R.string.mati)) Duracio = 1;
                else if(Temps == getResources().getString(R.string.tarda)) Duracio = 2;
                else Duracio = 3;
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
                Duracio = esdeveniment.getDaytime();
            }
        });



        final Button acceptar = v.findViewById(R.id.botoindicarajuda);
        acceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Titol = String.valueOf(TitolEvent.getText());
                String Descripcio = String.valueOf(DescripcioEvent.getText());
                String Data = String.valueOf(indicarData.getText());
                Context context = SplashActivity.getContext();
                SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                String userId = sharedPreferences.getString("userId", "0");

                if(localitzacio != null) {
                    esdeveniment.setLat(localitzacio.latitude);
                    esdeveniment.setLon(localitzacio.longitude);
                }
                Esdeveniment Esd = new Esdeveniment(Titol, esdeveniment.getLat(), esdeveniment.getLon(), Duracio, Data, userId, Descripcio);
                Esd.setId(esdeveniment.getId());
                try {
                    editEvent(Esd); //envia notificaio al ajudant!
                    acceptar.setVisibility(View.GONE);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }

    private void initSearchPlaces(View v) {
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), apiKey);
        }

        placesClient = Places.createClient(getContext());

        autocompleteSupportFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.supportFragment);
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME));
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                localitzacio = place.getLatLng();
            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });
    }
}
