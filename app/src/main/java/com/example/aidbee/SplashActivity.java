package com.example.aidbee;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.TimeUnit;

import static com.example.aidbee.UtilsSessions.refreshToken;

public class SplashActivity extends AppCompatActivity {

    private static AppCompatActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(1));
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        Intent intent = null;
        if (sharedPreferences.contains("idToken")) {
            String tokenId = sharedPreferences.getString("idToken", "0");
            Integer code = refreshToken();
            if (code.equals(200))
                intent = new Intent(this, Home.class);
            else intent = new Intent(this, PantallaLogin.class);
        }
        else {
            intent = new Intent(this, PantallaLogin.class);
        }
        startActivity(intent);
        finish();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }
}
