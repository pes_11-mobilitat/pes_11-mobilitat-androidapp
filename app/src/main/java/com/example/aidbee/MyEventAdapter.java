package com.example.aidbee;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyEventAdapter extends RecyclerView.Adapter<MyEventAdapter.EsdevenimentViewHolder>{

    private Context mCtx;
    private List<Esdeveniment> llistaEsdeveniments;
    private Dialog dialog;


    public MyEventAdapter(Context mCtx, List<Esdeveniment> llistaEsdeveniments, Dialog dialog) {
        this.mCtx = mCtx;
        this.llistaEsdeveniments = llistaEsdeveniments;
        this.dialog = dialog;
    }

    @NonNull
    @Override
    public EsdevenimentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //crear una instància de viewholder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.item_list_my_events, null);
        return new EsdevenimentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EsdevenimentViewHolder holder, int position) {
        //afegir dades al nostre viewholder
        try {
            Esdeveniment esdeveniment = llistaEsdeveniments.get(position);
            holder.textViewTitol.setText(esdeveniment.getTitle());
            holder.esdev = UtilsEsdeveniments.getEventPrivate(esdeveniment.getId());
            holder.esdev.setId(esdeveniment.getId());
        }
        catch (Exception ex) {

        }
//        holder.textViewDescripcio.setText(esdeveniment.getDescripcio());
//        Integer franja = esdeveniment.getDaytime();
//        String dayTime = null;
//        if (franja == 1) dayTime = "Matí";
//        else if (franja == 2) dayTime = "Tarda";
//        else if (franja == 3) dayTime = "Tot el dia";
//        holder.textViewFranja.setText(dayTime);
    }

    @Override
    public int getItemCount() {
        return llistaEsdeveniments.size();
    }

    class EsdevenimentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitol;
        Button buttonWatch;
        Esdeveniment esdev;
//        TextView textViewDescripcio;
//        TextView textViewFranja;

        public EsdevenimentViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitol = itemView.findViewById(R.id.itemMyEventTitle);
            buttonWatch = itemView.findViewById(R.id.btn_watch_my_event);
            buttonWatch.setOnClickListener(this);
//            textViewDescripcio = itemView.findViewById(R.id.itemMyEventDescription);
//            textViewFranja = itemView.findViewById(R.id.itemMyEventFranja);
        }

        @Override
        public void onClick(View v) {
            FragmentManager fm = ((AppCompatActivity)mCtx).getSupportFragmentManager();
            VisualitzarActivitatPlanejadaMaps fragment = new VisualitzarActivitatPlanejadaMaps(esdev);
            //fm.beginTransaction().remove(fm.findFragmentById(R.id.nav_host_fragment)).add(R.id.nav_host_fragment, fragment).commit();
            fm.beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();
            dialog.dismiss();

        }
    }

}
