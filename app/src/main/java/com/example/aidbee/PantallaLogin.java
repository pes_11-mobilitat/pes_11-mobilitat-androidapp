package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static com.example.aidbee.UtilsSessions.setDeviceToken;

public class PantallaLogin extends AppCompatActivity {
    Button botoIniciSessio, botoRegistre;
    EditText textMail, textContrasenya;

    private static AppCompatActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.pantalla_login);

        Toolbar toolbar = findViewById(R.id.loginToolbar);
        initToolbar(toolbar);

        botoIniciSessio = findViewById(R.id.botoIniciSessio);
        botoRegistre = findViewById(R.id.botoRegistre);
        textMail = findViewById(R.id.loginMail);
        textContrasenya = findViewById(R.id.loginContrasenya);


        botoIniciSessio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail = String.valueOf(textMail.getText());
                String pass = String.valueOf(textContrasenya.getText());
                if (mail.isEmpty()) {
                    textMail.setError(getResources().getString(R.string.indica_mail));
                    textMail.requestFocus();
                    return;
                }
                else if (pass.isEmpty()) {
                    textContrasenya.setError(getResources().getString(R.string.indica_contrasenya));
                    textContrasenya.requestFocus();
                    return;
                }
                else {
                    //fer login usuari
                    int res = UtilsSessions.loginUsuari(mail,pass);
                    if (res == -1)  {
                        //aquí tractem l'error amb una pantalla pop-up
                        LayoutInflater inflater = (LayoutInflater)
                                getSystemService(LAYOUT_INFLATER_SERVICE);
                        View popupView = inflater.inflate(R.layout.popup_window, null);
                        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                        boolean focusable = true; // lets taps outside the popup also dismiss it
                        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

                        // show the popup window
                        // which view you pass in doesn't matter, it is only used for the window tolken
                        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                        popupView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                popupWindow.dismiss();
                                return true;
                            }
                        });

                    }
                    else {
                        FirebaseInstanceId.getInstance().getInstanceId()
                                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                        if (!task.isSuccessful()) {
                                            return;
                                        }

                                        // Get new Instance ID token
                                        String deviceToken = task.getResult().getToken();
                                        try {
                                            setDeviceToken(deviceToken);
                                        } catch (AuthenticatorException e) {
                                            e.printStackTrace(); //GO LOGIN
                                        }
                                    }
                                });
                        Intent intent = new Intent(PantallaLogin.this, Home.class);
                        startActivity(intent);
                    }
                }
            }
        });
        botoRegistre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PantallaLogin.this, RegistreUsuari.class)); //això hauria de canviar a la pantalla de registre
            }
        });
    }

    private void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled();
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle("");
        //Personalitzar buttton back: https://stackoverflow.com/questions/26651602/display-back-arrow-on-toolbar
    }
    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        Intent intent = getIntent();
        if(intent.getBooleanExtra(Home.SHOW_LOGGED_OUT_EXTRA, false)){
            Toast.makeText(getContext(), getResources().getString(R.string.toast_logged_out), Toast.LENGTH_LONG).show();
        }
        super.onPostCreate(savedInstanceState);
    }
}
