package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.aidbee.ui.home.HomeFragment;

/**
 * A simple {@link Fragment} subclass.
 */

public class ValorarAjudant extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.valorarajudant, container, false);
        final ImageButton votpositiu = v.findViewById(R.id.votpositiu);
        final ImageButton votnegatiu = v.findViewById(R.id.votnegatiu);
        String Id = null;
        try {
            Id = utilsMatch.getIdMatch();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        final String finalId1 = Id;
        votpositiu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(0);
                FragmentManager fm = getFragmentManager();
                HomeFragment homeFragment = new HomeFragment();
                fm.beginTransaction().replace(R.id.home_layout, homeFragment).commit();
                try {
                    utilsMatch.endMatch(finalId1,"1");
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
            }
        });
        final String finalId = Id;
        votnegatiu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(0);
                FragmentManager fm = getFragmentManager();
                HomeFragment homeFragment = new HomeFragment();
                fm.beginTransaction().replace(R.id.home_layout, homeFragment).commit();
                try {
                    utilsMatch.endMatch(finalId,"-1");
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }
}
