package com.example.aidbee;

public class AjudaImmediata {

    private String request_id;
    private Double created_at;
    private Boolean accepted;
    private String upm_id;
    private Double location_lat;
    private Double location_lon;
    private Boolean urgent;
    private Double distance;
    private String request_type;
    private Integer estimated_duration;

    public AjudaImmediata() {
        request_id = null;
        created_at = null;
        accepted = null;
        upm_id = null;
        location_lat = null;
        location_lon = null;
        urgent = null;
        distance = null;
        request_type = null;
        estimated_duration = null;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public Double getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Double created_at) {
        this.created_at = created_at;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getUpm_id() {
        return upm_id;
    }

    public void setUpm_id(String upm_id) {
        this.upm_id = upm_id;
    }

    public Double getLocation_lat() {
        return location_lat;
    }

    public void setLocation_lat(Double location_lat) {
        this.location_lat = location_lat;
    }

    public Double getLocation_lon() {
        return location_lon;
    }

    public void setLocation_lon(Double location_lon) {
        this.location_lon = location_lon;
    }

    public Boolean getUrgent() {
        return urgent;
    }

    public void setUrgent(Boolean urgent) {
        this.urgent = urgent;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public Integer getEstimated_duration() {
        return estimated_duration;
    }

    public void setEstimaated_duration(Integer estimated_duration) {
        this.estimated_duration = estimated_duration;
    }
}
