package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import androidx.recyclerview.widget.RecyclerView;

import com.example.aidbee.ui.events.EventsFragment;
import com.example.aidbee.ui.home.HomeFragment;
import com.google.android.gms.maps.model.LatLng;


import java.text.DecimalFormat;
import java.util.List;

public class EsdevenimentAdapter extends RecyclerView.Adapter<EsdevenimentAdapter.EsdevenimentViewHolder>{

    private Context mCtx;
    private List<Esdeveniment> LlistaEsdeveniments;


    public EsdevenimentAdapter(Context mCtx, List<Esdeveniment> llistaEsdeveniments) {
        this.mCtx = mCtx;
        LlistaEsdeveniments = llistaEsdeveniments;
    }

    @NonNull
    @Override
    public EsdevenimentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //crear una instància de viewholder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.llista_esdeveniments, null);
        return new EsdevenimentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EsdevenimentViewHolder holder, int position) {
        //afegir dades al nostre viewholder
        Esdeveniment esdeveniment = LlistaEsdeveniments.get(position);
        Context context = SplashActivity.getContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "0");

        holder.textViewTitol.setText(esdeveniment.getTitle());
        holder.textViewData.setText(esdeveniment.getDate());

        try {
            LatLng ubi_usuari = UtilsUbicacio.getUbicacio(userId);
            DecimalFormat f = new DecimalFormat("##.00");
            holder.textViewDistancia.setText(String.valueOf(f.format(calculatRange(esdeveniment.getLat(),esdeveniment.getLon(),ubi_usuari.latitude,ubi_usuari.longitude))) + " Km");
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return LlistaEsdeveniments.size();
    }

    class EsdevenimentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textViewTitol;
        TextView textViewData;
        TextView textViewDistancia;
        public EsdevenimentViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textViewTitol = itemView.findViewById(R.id.textViewTitle);
            textViewData = itemView.findViewById(R.id.Data);
            textViewDistancia = itemView.findViewById(R.id.Distancia);
        }
        @Override
        public void onClick(View view) {

            // here you can get your item by calling getAdapterPosition();
            Esdeveniment ev =  LlistaEsdeveniments.get(getAdapterPosition());
            EventsFragment.canviarPantalla(getAdapterPosition(),view);
        }
    }

    public double calculatRange(double lat1, double lon1, double lat2, double lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            double radlat1 = Math.PI * lat1 / 180;
            double radlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double radtheta = Math.PI * theta / 180;
            double dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return dist;
        }
    }

}
