package com.example.aidbee;

public class FactoryStatusApplication {
    private static final FactoryStatusApplication ourInstance = new FactoryStatusApplication();
    private StatusApplication statusApplication;
    private Boolean firstTimeConnected;

    public static FactoryStatusApplication getInstance() {
        return ourInstance;
    }

    private FactoryStatusApplication() {
        firstTimeConnected = true;
    }

    public void createStatusApplication(int code) {
        statusApplication = new StatusApplication(code);
        firstTimeConnected = false;
    }

    public StatusApplication getStatusApplication() {
        return statusApplication;
    }

    public Boolean getFirstTimeConnected() {
        return firstTimeConnected;
    }
}
