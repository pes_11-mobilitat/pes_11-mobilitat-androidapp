package com.example.aidbee;

import android.accounts.AuthenticatorException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.aidbee.ui.home.HomeFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.aidbee.UtilsSessions.*;
import static com.example.aidbee.ui.home.HomeFragment.setGoneVisibilityBuscarButton;
import static com.example.aidbee.ui.home.HomeFragment.setGoneVisibilityCrearEvent;
import static com.example.aidbee.utilsMatch.getIdVoluntariMatch;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback {
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    AjudaImmediata aI;
    private FloatingActionButton xat;
    private FloatingActionButton tancar;
    private Usuari user;

    private String userId;

    public MapsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = Home.user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_maps, container, false);
        tancar = v.findViewById(R.id.tancar);
        final LinearLayout info = v.findViewById(R.id.infoMatchLayout);

        String rol = user.getRol();
        if (rol.equals("Ajudant")) {
            tancar.hide();
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        tancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setTitle(getResources().getString(R.string.acabar_ajuda));
                // R.string.app_name
                builder.setMessage(getResources().getString(R.string.fi_ajuda));
                builder.setPositiveButton(getResources().getString(R.string.acceptar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FragmentManager fm = getFragmentManager();
                        ValorarAjudant valorarajudant = new ValorarAjudant();
                        fm.beginTransaction().replace(R.id.map, valorarajudant).commit();
                        tancar.hide();
                        xat.hide();
                        info.setVisibility(View.GONE);
                        dialog.dismiss();
                    }

                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }

                });
                android.app.AlertDialog alert = builder.create();
                alert.show();


            }
        });


        xat = v.findViewById(R.id.xat);
        xat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                XatFragment xatFragment = new XatFragment();
                transaction.replace(R.id.home_layout, xatFragment).commit();
                transaction.addToBackStack(null);

            }
        });
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        String rol;
        try {
            rol = user.getRol();
            Location location = Home.getActualLocation();
            LatLng myLoc = new LatLng(location.getLatitude(), location.getLongitude());

            //LatLng otherLoc = new LatLng(30, -120);
            LatLng otherLoc;
            if (rol.equals("UPM")) {
                userId = getIdVoluntariMatch();
                otherLoc = UtilsUbicacio.getUbicacio(userId);
                //otherLoc = new LatLng(41.41, 2.15);
                //AQUI ALGO NO XUTA
                getActualPositionUMPToMarker(myLoc);
                getActualPositionAjudantToMarker(otherLoc);

            }
            else {
                aI = HomeFragment.getAjudaImmediata(); // NOMES EL AJUDANT TE AJUDA IMMEDIATA
                userId = aI.getUpm_id();
                otherLoc = UtilsUbicacio.getUbicacio(userId);
                getActualPositionUMPToMarker(otherLoc);
                getActualPositionAjudantToMarker(myLoc);
                FactoryStatusApplication.getInstance().getStatusApplication().setStatusCode(5);
            }
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
    }

    private void getActualPositionAjudantToMarker(LatLng loc) {
        //LatLng randomLocation = new LatLng(30, -120);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(loc);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        markerOptions.title(getResources().getString(R.string.pos_ajudant));

        //mMap.animateCamera(CameraUpdateFactory.newLatLng(randomLocation));
        mMap.addMarker(markerOptions);
    }

    private void getActualPositionUMPToMarker(LatLng loc) {
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.logo_aidbee);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, 84+84, 84+84, false);

        //Location location = Home.getActualLocation();
        //LatLng randomLocation = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(loc);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        markerOptions.title(getResources().getString(R.string.pos_upm));
        //markerOptions.snippet("My Postion");

        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(loc));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 10.0f ) );
        mMap.addMarker(markerOptions);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView altrePersona = getView().findViewById(R.id.altrePersona);
        TextView altrePersonaInput = getView().findViewById(R.id.altrePersonaInput);
        TextView disc = getView().findViewById(R.id.disc);
        TextView durada = getView().findViewById(R.id.durada);
        TextView descripcioAjuda = getView().findViewById(R.id.descripcioAjuda);

        setGoneVisibilityBuscarButton();
        setGoneVisibilityCrearEvent();

        try {
            String messageGetMatch = utilsMatch.getMatch();
            JSONObject jsonMsg = new JSONObject(messageGetMatch);
            if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() % 2 == 0) {
                //UPM
                altrePersona.setText(getResources().getString(R.string.voluntari));
                Usuari voluntari = getUser(jsonMsg.getString("uid"));
                altrePersonaInput.setText(voluntari.getNom());
                Home.nomUserMatch = voluntari.getNom();

            }
            else {
                //Ajudant
                altrePersona.setText(getResources().getString(R.string.upm));
                Usuari upm = getUser(jsonMsg.getString("upm_id"));
                altrePersonaInput.setText(String.valueOf(upm.getNom()));
                disc.setText(upm.getDiscapacitat());
                Home.nomUserMatch = upm.getNom();
            }
            durada.setText(jsonMsg.getString("estimated_duration"));
            descripcioAjuda.setText(translateTipusAjuda(jsonMsg.getString("request_type")));
            // TODO: translate tipus d'ajuda :D
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
    }

    private String translateTipusAjuda(String tipusAjuda) {
        String ret;
        switch (tipusAjuda) {
            case "Anar en metro":
                ret = getResources().getString(R.string.transport);
                break;
            case "Altres":
                ret = getResources().getString(R.string.altres);
                break;
            case "Fer la compra":
                ret = getResources().getString(R.string.fer_la_compra);
                break;
            case "Anar al metge":
                ret = getResources().getString(R.string.anar_al_metge);
                break;
            default:
                ret = "-";
                break;
        }
        return ret;
    }
}
