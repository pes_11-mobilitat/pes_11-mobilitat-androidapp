package com.example.aidbee.ui.events;

import android.accounts.AuthenticatorException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aidbee.Esdeveniment;
import com.example.aidbee.EsdevenimentAdapter;
import com.example.aidbee.R;
import com.example.aidbee.SplashActivity;
import com.example.aidbee.UtilsEsdeveniments;
import com.example.aidbee.UtilsUbicacio;
import com.example.aidbee.VisualitzarActivitatPlanejadaMaps;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class EventsFragment extends Fragment {

    private EventsViewModel eventsViewModel;
    RecyclerView recyclerView;
    EsdevenimentAdapter adapter1;
    Spinner s, s1, s2;
    FragmentManager fm = getFragmentManager();
    int check = 0;
    int check1 = 0;
    int check2 = 0;

    static List<Esdeveniment> LlistaEsdeveniments;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        eventsViewModel =
                ViewModelProviders.of(this).get(EventsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_events, container, false);

        LlistaEsdeveniments = new ArrayList<>();

        recyclerView = (RecyclerView) root.findViewById(R.id.llistaEsdeveniments);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        String[] arraySpinner = new String[] {
                getResources().getString(R.string.avui), getResources().getString(R.string.dema), getResources().getString(R.string.Qualsevol_dia)
        };
        s = (Spinner) root.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);

        String[] arraySpinner1 = new String[] {
                "1km", "5km", "10km", getResources().getString(R.string.qualsevol)
        };
        s1 = (Spinner) root.findViewById(R.id.spinner3);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arraySpinner1);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(adapter2);

        String[] arraySpinner2 = new String[] {
                getResources().getString(R.string.mati), getResources().getString(R.string.tarda), getResources().getString(R.string.totdia)
        };
        s2 = (Spinner) root.findViewById(R.id.spinner4);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arraySpinner2);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s2.setAdapter(adapter3);
        final ArrayList<Esdeveniment>[] arrayEv = new ArrayList[]{new ArrayList<Esdeveniment>()};
        try {
            arrayEv[0] = UtilsEsdeveniments.getEventsFiltered(null,null,null,null);
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }


        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String data = null;
                String text = s.getSelectedItem().toString();
                if (text.equals(getResources().getString(R.string.avui))) {
                    Date cDate = new Date();
                    data = new SimpleDateFormat("dd-MM-yyyy").format(cDate);
                }
                else if (text.equals(getResources().getString(R.string.dema))) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                    data = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());

                }
                else if (text.equals(getResources().getString(R.string.Qualsevol_dia))) {
                    data = null;
                }
                try {
                    if (++check > 1) {
                        arrayEv[0] = UtilsEsdeveniments.getEventsFiltered(data, null, null, null);

                        LlistaEsdeveniments.clear();
                        Iterator<Esdeveniment> iterator = arrayEv[0].iterator();
                        while (iterator.hasNext()) {
                            LlistaEsdeveniments.add(iterator.next());
                        }
                        adapter1 = new EsdevenimentAdapter(getActivity(), LlistaEsdeveniments);

                        recyclerView.setAdapter(adapter1);
                    }


                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Context context = SplashActivity.getContext();
                SharedPreferences sharedPreferences = context.getSharedPreferences("tokens", Context.MODE_PRIVATE);
                String userId = sharedPreferences.getString("userId", "0");
                Double distancia = null;
                String text = s1.getSelectedItem().toString();
                if (text.equals("1km")) {
                    distancia = 1.00;
                }
                else if (text.equals("5km")) {
                    distancia = 5.00;
                }
                else if (text.equals("10km")) {
                    distancia = 10.00;
                }
                else if (text.equals(getResources().getString(R.string.qualsevol))) {
                    distancia = null;
                }
                try {
                    if (++check1 > 1) {

                        LatLng ubi_usuari = UtilsUbicacio.getUbicacio(userId);
                        arrayEv[0] = UtilsEsdeveniments.getEventsFiltered(null, null, ubi_usuari, distancia);
                        LlistaEsdeveniments.clear();
                        Iterator<Esdeveniment> iterator = arrayEv[0].iterator();
                        while (iterator.hasNext()) {
                            LlistaEsdeveniments.add(iterator.next());
                        }
                        adapter1 = new EsdevenimentAdapter(getActivity(), LlistaEsdeveniments);

                        recyclerView.setAdapter(adapter1);
                    }
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        s2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                int daytime = 1;
                String text = s2.getSelectedItem().toString();
                if (text.equals(getResources().getString(R.string.mati))) {
                    daytime = 1;
                }
                else if (text.equals(getResources().getString(R.string.tarda))) {
                    daytime = 2;
                }
                else if (text.equals(getResources().getString(R.string.totdia))) {
                    daytime = 3;
                }
                try {
                    if (++check2 > 1) {
                        arrayEv[0] = UtilsEsdeveniments.getEventsFiltered(null, daytime, null, null);
                        LlistaEsdeveniments.clear();
                        Iterator<Esdeveniment> iterator = arrayEv[0].iterator();
                        while (iterator.hasNext()) {
                            LlistaEsdeveniments.add(iterator.next());
                        }
                        adapter1 = new EsdevenimentAdapter(getActivity(), LlistaEsdeveniments);

                        recyclerView.setAdapter(adapter1);

                    }
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        //Carregar els esdeveniments
        Iterator<Esdeveniment> iterator = arrayEv[0].iterator();
        while (iterator.hasNext()) {
            //System.out.println(iterator.next().getTitle());
            //Log.i("MyActivity",iterator.next().getTitle());
            LlistaEsdeveniments.add(iterator.next());
        }


        adapter1 = new EsdevenimentAdapter(getActivity(), LlistaEsdeveniments);

        recyclerView.setAdapter(adapter1);


        return root;
    }

    @SuppressLint("ResourceType")
    public static void canviarPantalla(int pos, View view) {
        Esdeveniment ev =  LlistaEsdeveniments.get(pos);
        FragmentActivity activity = (FragmentActivity)view.getContext();
        FragmentManager manager = activity.getSupportFragmentManager();

        VisualitzarActivitatPlanejadaMaps activitat = new VisualitzarActivitatPlanejadaMaps(ev);
        manager.beginTransaction().replace(R.id.esdeveniments_layout, activitat).commit();
    }
}