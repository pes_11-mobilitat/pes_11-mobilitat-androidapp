package com.example.aidbee.ui.valoracions;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ValoracionsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ValoracionsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is valoracions fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}