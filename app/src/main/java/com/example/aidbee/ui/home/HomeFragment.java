package com.example.aidbee.ui.home;

import android.accounts.AuthenticatorException;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.example.aidbee.AjudaImmediata;
import com.example.aidbee.CrearEventCalendari;
import com.example.aidbee.FactoryStatusApplication;
import com.example.aidbee.Home;
import com.example.aidbee.IndicarTipusAjuda;
import com.example.aidbee.MapsFragment;
import com.example.aidbee.R;
import com.example.aidbee.Usuari;
import com.example.aidbee.VisualitzarActivitatPlanejadaMaps;
import com.google.android.gms.maps.model.LatLng;

import static com.example.aidbee.UtilsSessions.getUser;
import static com.example.aidbee.utilsMatch.acceptRequest;
import static com.example.aidbee.utilsMatch.getNearRequest;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private static AjudaImmediata ajudaImmediata;
    private Button botoAcceptar;
    private LinearLayout layoutInfoReqUPM;
    private TextView nomUpm;
    private TextView discapacitatUpm;
    private TextView duracio;
    private TextView tipusReq;
    static Button botoBuscarAjuda, botoCrearEvent;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        botoBuscarAjuda = root.findViewById(R.id.botoconfirmacio);
        final Button botoGetNearReq = root.findViewById(R.id.botoRequest);
        botoAcceptar = root.findViewById(R.id.botoAcceptar);
        botoCrearEvent = root.findViewById(R.id.botoCrearEvent);
        TextView textWaitReq = root.findViewById(R.id.textWaitReq);
        nomUpm = root.findViewById(R.id.nomUpm);
        discapacitatUpm = root.findViewById(R.id.discapacitatUpm);
        duracio = root.findViewById(R.id.duracioReq);
        tipusReq = root.findViewById(R.id.tipusReq);
        layoutInfoReqUPM = root.findViewById(R.id.infoUPMContainer);

        textWaitReq.setPadding(20,200,20,0);
        textWaitReq.setText(getResources().getString(R.string.waiting_req));

        /*Usuari user = null;
        try {
            user = getUser();

        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        String rol = user.getRol();*/
        Integer status = FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode();
        String rol;
        if (status % 2 == 0) {
            rol = "UPM";
        }
        else rol = "Ajudant";

        layoutInfoReqUPM.setVisibility(View.GONE);
        if (rol.equals("UPM")) {
            textWaitReq.setVisibility(View.GONE);
            botoGetNearReq.setVisibility(View.GONE);
            botoAcceptar.setVisibility(View.GONE);
            botoBuscarAjuda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View root) {

                    /*
                    // TEST //
                    FragmentManager fm = getFragmentManager();
                    VisualitzarActivitatPlanejadaMaps mapsFragment = new VisualitzarActivitatPlanejadaMaps();
                    fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
                    */
                    FragmentManager fm = getFragmentManager();
                    IndicarTipusAjuda ajudaFragment = new IndicarTipusAjuda();
                    //fm.beginTransaction().attach(ajudaFragment).commit();
                    botoBuscarAjuda.setVisibility(View.GONE);
                    botoCrearEvent.setVisibility(View.GONE);
                    fm.beginTransaction().replace(R.id.home_layout, ajudaFragment).commit();
                }
            });
            botoCrearEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View root) {
                    FragmentManager fm = getFragmentManager();
                    CrearEventCalendari ajudaFragment = new CrearEventCalendari();
                    //fm.beginTransaction().attach(ajudaFragment).commit();
                    botoBuscarAjuda.setVisibility(View.GONE);
                    botoCrearEvent.setVisibility(View.GONE);
                    fm.beginTransaction().replace(R.id.home_layout, ajudaFragment).commit();
                }
            });
        }
        else {
            botoBuscarAjuda.setVisibility(View.GONE);
            botoCrearEvent.setVisibility(View.GONE);
            botoGetNearReq.setVisibility(View.GONE);
            botoAcceptar.setVisibility(View.GONE);
            //Comprovar si el status es 3 (el que es fa en el home) i borrar el del home i si es 3 mostrar el boto acceptar i la info, sino mostrar un text que digui "esperant requests..."
            if (FactoryStatusApplication.getInstance().getStatusApplication().getStatusCode() == 3) {
                mostrarInfoReqUPM();
                textWaitReq.setVisibility(View.GONE);

            }

            botoAcceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View root) {
                    String req_id = ajudaImmediata.getRequest_id();
                    int status = -1;
                    try {
                        status = acceptRequest(req_id);
                    } catch (AuthenticatorException e) {
                        e.printStackTrace(); //anar login
                    }
                    if (status==0) {
                        FragmentManager fm = getFragmentManager();
                        MapsFragment mapsFragment = new MapsFragment();
                        fm.beginTransaction().replace(R.id.home_layout, mapsFragment).commit();
                        botoAcceptar.setVisibility(View.GONE);
                    }
                }
            });
        }
        return root;
    }

    private void mostrarInfoReqUPM() {
        try {
            layoutInfoReqUPM.setVisibility(View.VISIBLE);
            Location location = Home.getActualLocation();
            LatLng myLoc = new LatLng(location.getLatitude(), location.getLongitude());
            ajudaImmediata = getNearRequest(myLoc);
            if (ajudaImmediata.getRequest_id() != null) {
                botoAcceptar.setVisibility(View.VISIBLE);
                Usuari upm = getUser(ajudaImmediata.getUpm_id());
                nomUpm.setText(upm.getNom());
                discapacitatUpm.setText(upm.getDiscapacitat());
                duracio.setText(String.valueOf(ajudaImmediata.getEstimated_duration()));
                tipusReq.setText(String.valueOf(ajudaImmediata.getRequest_type()));
            }
        } catch (AuthenticatorException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    static public AjudaImmediata getAjudaImmediata() {
        return ajudaImmediata;
    }

    public void setAjudaImmediata(AjudaImmediata ajudaImmediata) {
        this.ajudaImmediata = ajudaImmediata;
    }

    static public void setGoneVisibilityBuscarButton() {
        botoBuscarAjuda.setVisibility(View.GONE);
    }

    static public void setGoneVisibilityCrearEvent() {
        botoCrearEvent.setVisibility(View.GONE);
    }

}