package com.example.aidbee.ui.valoracions;

import android.accounts.AuthenticatorException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.aidbee.R;
import com.example.aidbee.Usuari;
import com.example.aidbee.ui.history.HistoryViewModel;

import static com.example.aidbee.UtilsSessions.getUser;

public class ValoracionsFragment extends Fragment {

    private ValoracionsViewModel valoracionsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_valoracions, container, false);
        final TextView textView = root.findViewById(R.id.puntuaciopos);
        final TextView textNeg = root.findViewById(R.id.puntuacioneg);
        final TextView textBalanç = root.findViewById(R.id.puntuacio);
        //
        //aquí cal cridar a les valoracions positives de l'usuari
        try {
            Usuari user = getUser();
            String pos = user.getUpvotes();
            String neg= user.getDownvotes();
            int ppos = Integer.parseInt(pos);
            int pneg = Integer.parseInt(neg);
            int b = ppos - pneg;
            textView.setText(pos);
            textNeg.setText(neg);
            textBalanç.setText(Integer.toString(b));

        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
        //textView.setText(pos);

        return root;
    }
}