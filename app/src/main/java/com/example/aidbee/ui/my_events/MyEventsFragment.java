package com.example.aidbee.ui.my_events;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aidbee.Esdeveniment;
import com.example.aidbee.MyEventAdapter;
import com.example.aidbee.R;
import com.example.aidbee.UtilsEsdeveniments;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;


public class MyEventsFragment extends Fragment implements OnDateSelectedListener {

//    private MyEventsViewModel myEventsViewModel;
    RecyclerView recyclerView;
    MyEventAdapter adapter;

    List<Esdeveniment> llistaEsdeveniments;
    MaterialCalendarView calendarView;

    Collection<CalendarDay> dates;
    HashMap<CalendarDay, Collection<Esdeveniment>> datesEsdeveniments = new HashMap<CalendarDay, Collection<Esdeveniment>>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        myEventsViewModel =
//                ViewModelProviders.of(this).get(MyEventsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_my_events, container, false);


        calendarView = (MaterialCalendarView) root.findViewById(R.id.calendarView);

        llistaEsdeveniments = new ArrayList<>();
        try {
            llistaEsdeveniments = UtilsEsdeveniments.getEventsUPM();
            llistaEsdeveniments.addAll(UtilsEsdeveniments.getEventsVoluntari());
        }
        catch(Exception ex){

        }

        dates = new ArrayList<>();
        for(Esdeveniment e : llistaEsdeveniments){
            try{
                Date date =new SimpleDateFormat("dd-MM-yyyy").parse(e.getDate());
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
                cal.setTime(date);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                CalendarDay cd = CalendarDay.from(year,month+1,day);
                dates.add(cd);
                Collection<Esdeveniment> tmp;
                if(datesEsdeveniments.containsKey(cd)){
                    tmp = datesEsdeveniments.get(cd);
                }
                else{
                    tmp = new ArrayList<>();
                }
                tmp.add(e);
                datesEsdeveniments.put(cd,tmp);
            }
            catch(Exception ex){

            }
        }
        calendarView.addDecorator(new EventDecorator(0,dates));
        calendarView.setOnDateChangedListener(this);

        return root;
    }


    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

        Collection<Esdeveniment> llistaEsdeveniments;
        if(datesEsdeveniments.containsKey(date)){
            llistaEsdeveniments = datesEsdeveniments.get(date);
            final Dialog dialog = new Dialog(getContext());

            View dialogView = getLayoutInflater().inflate(R.layout.my_events_list, null);
            RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.llistaEsdeveniments);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            adapter = new MyEventAdapter(getActivity(), new ArrayList(llistaEsdeveniments), dialog);
            recyclerView.setAdapter(adapter);

            dialog.setContentView(dialogView);
            dialog.setTitle("Title...");
            dialog.show();
        }

    }

    public class EventDecorator implements DayViewDecorator {

        private final int color;
        private final HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new DotSpan(5, color));
        }
    }

}