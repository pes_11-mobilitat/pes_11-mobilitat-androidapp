package com.example.aidbee;


import android.accounts.AuthenticatorException;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.aidbee.ui.home.HomeFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.example.aidbee.UtilsEsdeveniments.deleteEvent;
import static com.example.aidbee.UtilsEsdeveniments.joinEvent;
import static com.example.aidbee.UtilsEsdeveniments.leaveEvent;
import static com.example.aidbee.UtilsSessions.getUser;
import static com.example.aidbee.ui.home.HomeFragment.setGoneVisibilityBuscarButton;


public class VisualitzarActivitatPlanejadaMaps extends Fragment implements OnMapReadyCallback {
    private Usuari user;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private FloatingActionButton leaveEvent;
    private FloatingActionButton editar;
    private FloatingActionButton xat;
    private FloatingActionButton join;
    private FloatingActionButton deleteEvent;

    public static Esdeveniment esdeveniment;
    private LatLng localitzacio;

    private String rol;
    private boolean isAMatch;

    private LinearLayout linearLayoutInfo;

    public VisualitzarActivitatPlanejadaMaps() {
        // Required empty public constructor
        esdeveniment = null;
    }

    public VisualitzarActivitatPlanejadaMaps(Esdeveniment esdeveniment) {
        this.esdeveniment = esdeveniment;
        localitzacio = new LatLng(esdeveniment.getLat(), esdeveniment.getLon());
        if (esdeveniment.getVolunteer() == null) isAMatch = false;
        else isAMatch = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setGoneVisibilityBuscarButton();
        user = Home.user;
        rol = user.getRol();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_visualitzar_activitat_planejada_maps, container, false);

        linearLayoutInfo = v.findViewById(R.id.infoMatchLayout);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.pMap);
        if (mapFragment == null) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.pMap, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        leaveEvent = v.findViewById(R.id.pClose);
        xat = v.findViewById(R.id.pXat);
        editar = v.findViewById(R.id.pEditar);
        join = v.findViewById(R.id.pJoin);
        deleteEvent = v.findViewById(R.id.pDelete);

        leaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    leaveEvent(esdeveniment);
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
                hideAll();
                linearLayoutInfo.setVisibility(View.GONE);
                FragmentManager fm = getFragmentManager();
                HomeFragment homeFragment = new HomeFragment();
                fm.beginTransaction().replace(R.id.pMap, homeFragment).commit();
            }
        });

        xat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                XatFragment xatFragment = new XatFragment();
                transaction.replace(R.id.pMap, xatFragment).commit();
                transaction.addToBackStack(null);
                hideAll();
                linearLayoutInfo.setVisibility(View.GONE);
            }
        });

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                EditarEvent editarEvent = new EditarEvent(esdeveniment);
                transaction.replace(R.id.pMap, editarEvent).addToBackStack(null).commit();
                hideAll();
                linearLayoutInfo.setVisibility(View.GONE);
            }
        });

        deleteEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(getResources().getString(R.string.alerta));
                builder.setMessage(getResources().getString(R.string.esborrar_esd_conf));
                builder.setPositiveButton(getResources().getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    deleteEvent(esdeveniment);
                                } catch (AuthenticatorException e) {
                                    e.printStackTrace();
                                }
                                hideAll();
                                linearLayoutInfo.setVisibility(View.GONE);
                                FragmentManager fm = getFragmentManager();
                                HomeFragment homeFragment = new HomeFragment();
                                fm.beginTransaction().replace(R.id.pMap, homeFragment).commit();
                                dialog.dismiss();
                            }
                        });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    joinEvent(esdeveniment);
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
                //redirect als meus esdeveniments (join nomes ho crida voluntari
                if (rol.equals("Ajudant")) {
                    hideAll();
                    linearLayoutInfo.setVisibility(View.GONE);
                    FragmentManager fm = getFragmentManager();
                    HomeFragment homeFragment = new HomeFragment();
                    fm.beginTransaction().replace(R.id.pMap, homeFragment).commit();
                }
            }
        });

        hideAll();

        if (!isAMatch && rol.equals("Ajudant")) {
            join.show();
        }
        else if (!isAMatch && rol.equals("UPM")) {
            deleteEvent.show();
            editar.show();
        }
        else if (isAMatch) {
            xat.show();
            if (rol.equals("UPM")) {
                deleteEvent.show();
                editar.show();
            }
            else {
                leaveEvent.show();
            }
        }
        return v;
    }

    private void hideAll() {
        leaveEvent.hide();
        editar.hide();
        xat.hide();
        join.hide();
        deleteEvent.hide();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.logo_aidbee);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, 84+84, 84+84, false);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(localitzacio);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        markerOptions.title(getResources().getString(R.string.posicio));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(localitzacio));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(localitzacio));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 10.0f ) );
        mMap.addMarker(markerOptions);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView titolInput = getView().findViewById(R.id.titolInput);
        TextView franjaInput = getView().findViewById(R.id.franjaInput);
        TextView descInput = getView().findViewById(R.id.descInput);
        TextView volInput = getView().findViewById(R.id.volInput);

        try {
            if (esdeveniment.getVolunteer() != null) {
                Usuari user = getUser(esdeveniment.getVolunteer());
                volInput.setText(user.getNom());
                Home.nomUserMatch = user.getNom();
            }
            else {
                volInput.setText(getResources().getString(R.string.no_assignat));
            }
            titolInput.setText(esdeveniment.getTitle());
            String franja;
            if (esdeveniment.getDaytime().equals(1)) {
                franja = getResources().getString(R.string.mati);
            }
            else if (esdeveniment.getDaytime().equals(2)) {
                franja = getResources().getString(R.string.tarda);
            }
            else {
                franja = getResources().getString(R.string.totdia);
            }
            franjaInput.setText(franja);
            descInput.setText(esdeveniment.getDescripcio());

        } catch (AuthenticatorException e) {
            e.printStackTrace();
        }
    }
}
