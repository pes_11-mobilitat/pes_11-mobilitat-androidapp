package com.example.aidbee;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.aidbee.UtilsSessions.refreshToken;

public class ResumAjuda extends AppCompatActivity {
    Button botoAcceptar, botoDeclinar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int x = refreshToken();
        if (x == 403) {
            Intent intent = new Intent(ResumAjuda.this, PantallaLogin.class);
            startActivity(intent);
        }
        else {
            setContentView(R.layout.ajuda_resum);
        }

    }
}
